    Name : Chanikan Chamrat
    Nickname : Meen
    StudentID : 602115005

    Sleep Cycle/Sleep Tracking mobile appication 
    Name : KaiLub
    About application
        1. Stopwatch timer 
        2. Graph show sleep time and awake time 
        3. Show highest, lowest and average time 
        4. Calculate percentage of sleep per night
        5. Calculate sleep health 
        6. Create, edit status post to share tooltips about sleeping and delete post
        7. Update profile and profile image
        8. Register 
        9. Login
        10. Logout

    Initial UX/UI Design
        Figma link - https://www.figma.com/file/bZqcodaml02Y3wJXCTs2CN/Hackathon?node-id=96%3A914
    Research references 
        https://docs.google.com/document/d/1bB4JBps1UpV-F8Hc7FfUDiLZrx5AfroFMI1HeXWXfrQ/edit?usp=sharing