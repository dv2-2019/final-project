
import React from 'react';
import { StatusBar } from 'react-native';
import { DefaultTheme, configureFonts, Provider as PaperProvider } from 'react-native-paper';
import { SafeAreaProvider } from 'react-native-safe-area-context';
//persisist
import { PersistGate } from 'redux-persist/integration/react'
import Route from './src/routes/Route';
import AuthRoute from './src/routes/AuthRoute';
//redux
import { Provider } from 'react-redux';
import store, { persistor } from './src/store/store';
import EditProfile from './src/screens/EditProfilePage/EditProfile';
import SwipeStop from './src/screens/HomePage/SwipeStop';


const App = () => {

  const fontConfig = {
    default: {
      regular: {
        fontFamily: 'Poppins-Light',
        fontWeight: 'normal',
      },
      medium: {
        fontFamily: 'Poppins-Medium',
        fontWeight: 'normal',
      },
      light: {
        fontFamily: 'Poppins-Light',
        fontWeight: 'normal',
      },
      thin: {
        fontFamily: 'Poppins-Thin',
        fontWeight: 'normal',
      }
    }
  }

  const theme = {
    ...DefaultTheme,
    fonts: configureFonts(fontConfig),
    roundness: 2,
    colors: {
      ...DefaultTheme.colors,
      primary: '#46C7F0',
      accent: '#F7BB47',
    },
  }

  return (
    <Provider store={store} >
      <PersistGate loading={null} persistor={persistor}>

        <PaperProvider theme={theme}>
          <SafeAreaProvider>
            <StatusBar
              barStyle='light-content'
              translucent={true}
              backgroundColor='transparent' />
            <Route />
          </SafeAreaProvider>
        </PaperProvider>

      </PersistGate>
    </Provider>

  )
}


export default App;
