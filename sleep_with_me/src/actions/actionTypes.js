//get stat data
export const GET_STATDAY = 'GET_STATDAY';
export const GET_STATWEEK = 'GET_STATWEEK';
export const GET_STATMONTH = 'GET_STATMONTH';

//auth action
export const SET_AUTH_TOKEN = 'SET_AUTH_TOKEN';

//get user info
export const GET_USER_PROFILE = 'GET_USER_PROFILE';

