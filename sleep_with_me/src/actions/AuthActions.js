import { SET_AUTH_TOKEN } from './actionTypes';

export const getTokenUser = token => {
    return {
        type: SET_AUTH_TOKEN,
        payload: token
    }
}