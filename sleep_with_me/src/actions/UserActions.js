import { GET_USER_PROFILE} from './actionTypes';

export const getUserProfile = (data) => {
    return {
        type: GET_USER_PROFILE,
        payload: data
    }
}
