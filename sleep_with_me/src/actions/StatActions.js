import { GET_STATDAY, GET_STATWEEK, GET_STATMONTH } from './actionTypes';

export const getStatDayData = data => {
    return {
        type: GET_STATDAY,
        payload: data
    }
}
export const getStatWeekData = data => {
    return {
        type: GET_STATWEEK,
        payload: data
    }
}
export const getStatMonthData = data => {
    return {
        type: GET_STATMONTH,
        payload: data
    }
}