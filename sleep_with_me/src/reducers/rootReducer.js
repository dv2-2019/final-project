import { combineReducers } from 'redux';
import { statReducer } from './StatReducer';
import { authReducer } from './AuthReducer';
import { userReducer } from './UserReducer';

const rootReducer = combineReducers({
    statistics: statReducer,
    auth: authReducer,
    user: userReducer
})
export default rootReducer;
