
import { SET_AUTH_TOKEN } from '../actions/actionTypes';
const initialState = null

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_AUTH_TOKEN:
            return action.payload
        default:
            return state
    }
}