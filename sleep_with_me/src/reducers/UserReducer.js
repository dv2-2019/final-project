import { GET_USER_PROFILE } from '../actions/actionTypes';

const initialProfile = {}

export const userReducer = (state = initialProfile, action) => {
    switch (action.type) {
        case GET_USER_PROFILE:
            return action.payload
        default:
            return state
    }
}
