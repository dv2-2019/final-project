
import { GET_STATDAY, GET_STATWEEK, GET_STATMONTH } from '../actions/actionTypes';
const initialState = {
    statDay: {},
    statWeek: {},
    statMonth: {}
}
export const statReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_STATDAY:
            // console.log('get stat day ', action.payload)
            return {
                ...state,
                statDay: action.payload
            }
        case GET_STATWEEK:
            // console.log('get stat week ', action.payload)
            return {
                ...state,
                statWeek: action.payload
            }
        case GET_STATMONTH:
            // console.log('get stat month ', action.payload)
            return {
                ...state,
                statMonth: action.payload
            }
        default:
            return state
    }
}