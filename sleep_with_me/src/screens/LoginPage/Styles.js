import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#0F2D44'
    },
    imageBg: {
        width: 400,
    },
    textInput: {
        backgroundColor: '#19354B',
        width: 314,
        marginVertical: 8,
        fontSize: 14
    },
    textInWrap: {
        alignItems: 'center',
        paddingVertical: 8
    },
    submitBotton: {
        width: 174,
        elevation: 5,
        padding: 6,
        elevation: 0,
        marginTop: 14,
        borderRadius: 28,
        height: 52
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 32,
        marginBottom: 14
    },
    image: {
        width: 208,
        height: 192,
    },
    container: {
        flex: 1,
        backgroundColor: '#0F2D44',
        alignItems: 'center',
    },
    signLink: {
        color: '#F7BB47',
    },
    text: {
        color: '#ffff'
    },
    toSignUp: {
        flexDirection: 'row',
        marginTop: 28,
        justifyContent: 'center'
    }
})