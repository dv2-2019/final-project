import React, { useState, useRef } from 'react';
import { View, TouchableOpacity, Alert } from 'react-native';
import { Text, TextInput, Button } from 'react-native-paper';
import * as yup from 'yup'
import { Formik } from 'formik'
//css style
import styles from './Styles';
//redux
import { useNavigation } from '@react-navigation/native'
import { useDispatch } from 'react-redux';
import axios from 'axios';

export default LoginForm = () => {
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const passwordInput = useRef()
    const [isLoadingLogin, setIsLoadingLogin] = useState(false)

    return (
        <Formik
            initialValues={{
                username: '',
                password: '',
            }}
            //login form
            onSubmit={values => {
                console.log('Login = ', values)
                axios.post('http://34.234.203.236:8080/login', { username: values.username, password: values.password })
                    .then(response => {
                        //set token to reducer
                        dispatch({ type: 'SET_AUTH_TOKEN', payload: response.data.token })
                    })
                    .catch(error => {
                        console.log('error', error)
                    })
            }}

            validationSchema={yup.object().shape({
                username: yup
                    .string()
                    .required('Please, provide your name!'),
                password: yup
                    .string()
                    .min(6)
                    .max(10, 'Password should not excced 10 chars.')
                    .required(),
            })}
        >
            {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
                <View >
                    <TextInput
                        mode='outlined'
                        style={styles.textInput}
                        inlineImageLeft='user'
                        theme={{
                            roundness: 28,
                            colors: {
                                placeholder: '#A6A6A6',
                                underlineColor: 'transparent',
                                text: '#ffff',
                            }
                        }}
                        value={values.username}
                        onChangeText={handleChange('username')}
                        onBlur={() => setFieldTouched('username')}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => passwordInput.current.focus()}
                        placeholder='Username'
                    />
                    {touched.username && errors.username &&
                        <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.username}</Text>
                    }

                    <TextInput
                        ref={passwordInput}
                        mode='outlined'
                        style={styles.textInput}
                        theme={{
                            roundness: 28,
                            colors: {
                                placeholder: '#A6A6A6',
                                underlineColor: 'transparent',
                                text: '#ffff',
                            }
                        }}
                        value={values.password}
                        onChangeText={handleChange('password')}
                        onBlur={() => setFieldTouched('password')}
                        textContentType="password"
                        autoCapitalize="none"
                        placeholder='Password'
                        keyboardType="numeric"
                        secureTextEntry
                    />
                    {touched.password && errors.password &&
                        <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.password}</Text>
                    }

                    <View style={{ alignItems: 'center', marginVertical: 24 }}>
                        <Button
                            style={styles.submitBotton}
                            labelStyle={{ fontFamily: 'Poppins-Medium', color: '#ffff' }}
                            mode='contained'
                            loading={isLoadingLogin}
                            disabled={!isValid}
                            onPress={() => {
                                handleSubmit()
                            }}>
                            Login
                        </Button>
                    </View>
                </View>
            )}
        </Formik>

    )
}