import React from 'react';
import { View, Image, ImageBackground, TouchableOpacity, ScrollView } from 'react-native';
import { Text } from 'react-native-paper';
import SafeAreaView from 'react-native-safe-area-view';
import LoginForm from './LoginForm';
import { useNavigation } from '@react-navigation/native';
//css style
import styles from './Styles';

export default Login = () => {
    const navigation = useNavigation()

    return (
        <SafeAreaView style={styles.safeArea} forceInset={{ top: 'always' }}>
            <ScrollView>
                <View style={styles.container}>
                    <ImageBackground source={require('../../assets/login/bg_snow.png')} style={styles.imageBg}>
                        <View style={styles.logoContainer}>
                            <Image source={require('../../assets/login/login_logo.png')}
                                style={styles.image} />
                        </View>
                    </ImageBackground>
                    {/* login form */}
                    <LoginForm />

                    <View style={styles.toSignUp}>
                        <Text style={styles.text}>Don't have an account ?  </Text>
                        <TouchableOpacity
                            onPress={() => {
                                navigation.navigate('Register')
                            }}
                        >
                            <Text style={styles.signLink}>Sign Up</Text>
                        </TouchableOpacity >
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}