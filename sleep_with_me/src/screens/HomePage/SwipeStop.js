import React, { useState, useEffect } from 'react';
import { View, ImageBackground, Image, FlatList } from 'react-native';
import { Button, Text, Dialog, Portal, Paragraph } from 'react-native-paper';
import SafeAreaView from 'react-native-safe-area-view';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import Swiper from 'react-native-deck-swiper'
//css style
import styles from './Styles';
import { useSelector } from 'react-redux';
import axios from 'axios';

export default SwipeStop = () => {
    const navigation = useNavigation()
    const userInfo = useSelector(state => state.user)
    const token = useSelector(state => state.auth)

    //show time 
    const [currentTime, setCurrentTime] = useState({
        clock: null,
        timeFormat: null,
        todayDate: null
    })

    //watch timer
    const [dialogVisible, setDialogVisible] = useState(false)
    // 
    const [timer, setTimer] = useState(0)
    const [disPlayTimer, setDisPlayTimer] = useState(0)

    var seconds = ("0" + (Math.floor(disPlayTimer / 1000) % 60)).slice(-2)
    var minutes = ("0" + (Math.floor(disPlayTimer / 60000) % 60)).slice(-2)
    var hours = ("0" + Math.floor(disPlayTimer / 3600000)).slice(-2)

    //get curDate and time
    var date1 = new Date().toLocaleString().slice(0, 10)
    var time1 = Math.round((new Date()).getTime() / 1000) //keep as timestamp 

    //function
    const handleStart = () => {
        setDialogVisible(false)

        setTimer(timer + disPlayTimer)

        let startTimer = (new Date()).getTime()
        let interval = setInterval(() => {
            let curTime = null
            if (disPlayTimer != 0) {
                curTime = disPlayTimer + ((new Date()).getTime() - startTimer)
            } else {
                curTime = (new Date()).getTime() - startTimer
            }
            setDisPlayTimer(curTime)
        }, 1000)
        setTimer(interval)
        console.log("interval :", interval)
    }

    //stop and push data to db
    const handleStop = () => {
        console.log("disPlayTimer12312312312: ", disPlayTimer);
        console.log('Log Timer msec interval', timer)
        clearInterval(timer)

        let date2 = new Date().toLocaleString().slice(0, 10)
        let time2 = Math.round((new Date()).getTime() / 1000) //keep as timestamp

        if (minutes <= 30) {
            setDialogVisible(true)
        } else {
            // console.log('lap time:  ', hours + ':' + minutes + ':' + seconds)
            console.log('disPlayTimer', disPlayTimer)
            console.log('stop time: ', time2)
            console.log('stop date: ', date2)
            console.log('start time: ', time1)
            console.log('start date: ', date1)
            console.log('token : ', token)

            axios.post('http://34.234.203.236:8080/member/statistics', {
                lapTime: disPlayTimer,
                startTime: time1,
                stopTime: time2,
                startDate: date1,
                stopDate: date2
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }).then(response => {
                console.log('response completed add statistics', response.data)
                setDialogVisible(false)
                clearInterval(disPlayTimer)
                navigation.navigate('Home')
            }).catch(error => {
                console.log('error add sattistics : ', error)
            })
        }
    }

    //dialog
    const handleReset = () => {
        setDialogVisible(false)
        clearInterval(disPlayTimer)
        navigation.navigate('Home')
    }

    const getCurrentTime = () => {
        let hour = new Date().getHours();
        let minute = new Date().getMinutes();
        let am_pm = 'pm';
        let todayDate = new Date().toDateString()

        if (minute < 10) {
            minute = '0' + minute;
        }
        if (hour > 12) {
            hour = hour - 12;
        }
        if (hour == 0) {
            hour = 12;
        }
        if (new Date().getHours() < 12) {
            am_pm = 'am';
        }
        setCurrentTime({
            clock: hour + ":" + minute,
            timeFormat: am_pm,
            todayDate: todayDate
        })
    }

    useEffect(() => {
        setInterval(() => {
            getCurrentTime();
        }, 1000)

        handleStart()
    }, [])


    return (
        <SafeAreaView style={styles.safeArea} >
            <ImageBackground source={require('../../assets/home/bg_night_mountain_blur.png')} style={styles.imageBg}>

                <View style={styles.topContainerSwipe}>
                    <View >
                        <Text style={styles.headerText}>Good night, {userInfo.username}</Text>
                    </View>
                    {/* date and time */}
                    <View style={styles.timeContainer}>
                        <View style={styles.rowTime}>
                            <Text style={styles.clockTime}>{currentTime.clock}</Text>
                        </View>
                        <View style={styles.rowTime}>
                            <Text style={styles.pmam}>{currentTime.timeFormat}</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.date}>{currentTime.todayDate}</Text>
                    </View>
                </View>

                <View style={styles.timeCountContainer}>
                    <ImageBackground source={require('../../assets/home/bg_timer.png')} style={{ width: 216, height: 216, flexDirection: 'row' }}>
                        <View style={styles.timeIcon}>
                            <Ionicons size={24} color='#ffff' name='ios-timer'></Ionicons>
                        </View>
                        <View style={styles.showTime}>
                            <Text style={styles.showTimeText}>{hours}:{minutes}:{seconds}</Text>
                        </View>
                        <View style={styles.timeFormat} >
                            <Text style={styles.timeUnitText}>h</Text>
                        </View>
                    </ImageBackground>
                </View>

                {/* 
                <Swiper
                    ref={swiper => {
                        swiper = swiper
                    }}
                    verticalThreshold={320}
                    horizontalSwipe={false}
                    disableBottomSwipe={true}
                    cards={['Swipe up to stop']}
                    renderCard={(card) => {
                        return (
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                                <Image source={require('../../assets/home/arrow_up.png')} style={{ width: 28, height: 8, padding: 8 }} />
                                <Text style={{ color: '#ffff' }}>{card}</Text>
                            </View>
                        )
                    }}
                    onSwipedTop={() => { handleStop() }}
                    backgroundColor={'rgba(25, 55, 77, 0.20)'}
                >
                </Swiper> */}
                <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
                    <Button
                        onPress={() => handleStop()}>
                        stop
                </Button>
                </View>

                <Portal>
                    <Dialog
                        visible={dialogVisible}
                        onDismiss={() => { handleReset() }}
                        style={styles.dialogContainer}>
                        <Dialog.Content style={styles.dialogHeader}>
                            <Image source={require('../../assets/dialog_img.png')} style={styles.dialogImg}></Image>
                        </Dialog.Content>
                        <Dialog.Content style={styles.dialogContent}>
                            <Dialog.Title>Warning</Dialog.Title>
                            <Paragraph>Sleep monitors takes at least 30 mins to measure your sleep behavior.</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <View style={styles.dialogFooter}>
                                <Button
                                    uppercase={false}
                                    labelStyle={{ color: '#858585', fontSize: 13 }}
                                    onPress={() => {
                                        handleReset()
                                    }}>
                                    Quit
                                </Button>
                            </View>
                            <View style={styles.dialogFooter}>
                                <Button
                                    uppercase={false}
                                    style={styles.dialogBtn}
                                    labelStyle={{ color: '#ffff', fontSize: 13 }}
                                    onPress={() =>
                                        handleStart()
                                    }
                                >
                                    Continue
                                </Button>
                            </View>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>

            </ImageBackground>
        </SafeAreaView>
    )
}
