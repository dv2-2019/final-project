import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#0F2D44'
    },
    topContainer: {
        paddingHorizontal: 24,
        justifyContent: 'center',
        flex: 1
    },
    topContainerSwipe: {
        paddingHorizontal: 24,
        justifyContent: 'flex-end',
        flex: 1
    },
    headerText: {
        fontSize: 16,
        color: '#ffff',
        fontFamily: 'Poppins-Medium',
    },
    timeContainer: {
        flexDirection: 'row',
    },
    imageBg: {
        flex: 1
    },
    clockTime: {
        fontSize: 52,
        fontFamily: 'Poppins-Medium',
        color: '#ffff'
    },
    rowTime: {
        justifyContent: 'center',
        height: 80,
        paddingTop: 24,
    },
    date: {
        color: '#ffff',
        fontSize: 16,
    },
    pmam: {
        color: '#ffff',
        fontSize: 16,
        paddingLeft: 16
    },
    startContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    startBotton: {
        width: 164,
        elevation: 5,
        padding: 6,
        borderRadius: 28,
        height: 50,
    },
    startBtnBg: {
        width: 186,
        height: 72,
        alignItems: 'center',
        justifyContent: 'center',
    },
    stopContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    stopBotton: {
        backgroundColor: '#FF7269',
        width: 164,
        elevation: 5,
        padding: 6,
        borderRadius: 28,
        height: 50,
    },
    timeCountContainer: {
        flex: 2,
        alignItems: 'center',
        marginTop: 16
    },
    timeIcon: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    timeFormat: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    showTime: {
        flex: 3,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    showTimeText: {
        fontFamily: 'Poppins-Medium',
        color: '#ffff',
        fontSize: 24
    },
    timeUnitText: {
        color: '#ffff',
        fontSize: 16
    },
    //dialog 
    dialogContainer: {
        borderRadius: 14,
        backgroundColor: '#EBF7FF',
        elevation:6
    },
    dialogHeader: {
        backgroundColor: '#0F2D44',
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopRightRadius: 12,
        borderTopLeftRadius: 12,
    },
    dialogImg: {
        width: 100,
        height: 100
    },
    dialogContent: {
        alignItems: 'center',
        height: 128
    },
    dialogFooter: {
        marginHorizontal: 2
    },
    dialogBtn: {
        backgroundColor: '#46C7F0',
        borderRadius: 12,
        height: 38
    }



})