import React, { useState, useEffect } from 'react';
import { View, ImageBackground, Image, FlatList } from 'react-native';
import { Button, Text, Dialog, Portal, Paragraph } from 'react-native-paper';
import SafeAreaView from 'react-native-safe-area-view';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PushNotification from "react-native-push-notification";
import { useNavigation } from '@react-navigation/native';
//css style
import styles from './Styles';
import SwipeStop from './SwipeStop';
import { useSelector } from 'react-redux';

export default Home = () => {
    const navigation = useNavigation()
    const userInfo = useSelector(state => state.user)

    //show time 
    const [currentTime, setCurrentTime] = useState({
        clock: null,
        timeFormat: null,
        todayDate: null
    })

    const pushNotificationSleep = () => {
        var repeatAtTime = new Date()
        repeatAtTime.setHours(23, 0, 0)
        PushNotification.localNotificationSchedule({
            //... You can use all the options from localNotifications
            vibrate: true,
            vibration: 1000,
            title: "Sleepinest",
            message: "It's time to go to sleep!", // 
            date: repeatAtTime,
            repeatType: "day",
        })
    }
    const pushNotificationAwake = () => {
        var awakeAtTime = new Date()
        repeatAtTime.setHours(7, 0, 0)
        PushNotification.localNotificationSchedule({
            //... You can use all the options from localNotifications
            vibrate: true,
            vibration: 1000,
            title: "Sleepinest",
            message: "It's time get up, Good morning enjoy your day", // 
            date: awakeAtTime,
            repeatType: "day",
        })
    }

    const getCurrentTime = () => {
        let hour = new Date().getHours();
        let minute = new Date().getMinutes();
        let am_pm = 'pm';
        let todayDate = new Date().toDateString()

        if (minute < 10) {
            minute = '0' + minute;
        }
        if (hour > 12) {
            hour = hour - 12;
        }
        if (hour == 0) {
            hour = 12;
        }
        if (new Date().getHours() < 12) {
            am_pm = 'am';
        }
        setCurrentTime({
            clock: hour + ":" + minute,
            timeFormat: am_pm,
            todayDate: todayDate
        })
    }

    useEffect(() => {
        setInterval(() => {
            getCurrentTime();
        }, 1000)
        // pushNotificationSleep()
        // pushNotificationAwake()
    }, [])

    return (
        <SafeAreaView style={styles.safeArea} >
            <ImageBackground source={require('../../assets/home/bg_night_mountain.png')} style={styles.imageBg}>

                <View style={styles.topContainer}>
                    <View >
                        <Text style={styles.headerText}>Good night, {userInfo.username}</Text>
                    </View>
                    {/* date and time */}
                    <View style={styles.timeContainer}>
                        <View style={styles.rowTime}>
                            <Text style={styles.clockTime}>{currentTime.clock}</Text>
                        </View>
                        <View style={styles.rowTime}>
                            <Text style={styles.pmam}>{currentTime.timeFormat}</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.date}>{currentTime.todayDate}</Text>
                    </View>
                </View>

                <View style={styles.startContainer} >
                    <ImageBackground source={require('../../assets/home/bg_start_btn.png')}
                        style={styles.startBtnBg}>
                        <Button
                            style={styles.startBotton}
                            labelStyle={{ color: '#ffff', fontSize: 14 }}
                            mode='contained'
                            uppercase={false}
                            onPress={() => {
                                navigation.navigate('SwipeStop')
                            }}>
                            <Icon size={16} name='caretright'></Icon>
                                         Start Sleep
                                    </Button>
                    </ImageBackground>
                </View>

            </ImageBackground>
        </SafeAreaView>
    )
}