import React, { useState } from 'react';
import { View, Image, TouchableOpacity, ScrollView } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import ImagePicker from 'react-native-image-picker';
import EditForm from './EditForm';
//css style
import styles from './Styles';
import axios from 'axios';
import { useSelector } from 'react-redux';
export default EditProfile = () => {
    const [avatarSource, setAvatarSource] = useState('')
    const token = useSelector(state => state.auth)
    const userInfo = useSelector(state => state.user)

    const [photo, setPhoto] = useState('https://res.cloudinary.com/dbripdcme/image/upload/v1581387688/m0e7y6s5zkktpceh2moq.jpg');

    const selectPhotoTapped = () => {
        const options = {
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {

            // console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const uri = response.uri;
                const type = response.type;
                const name = response.fileName;
                const source = {
                    uri,
                    type,
                    name,
                }
                cloudinaryUpload(source)
                setAvatarSource(source)
            }
        });
    }
    const cloudinaryUpload = (photo) => {
        const data = new FormData()
        data.append('file', photo)
        data.append('upload_preset', 'ti6kszim')
        data.append("cloud_name", "dbripdcme")
        fetch("https://api.cloudinary.com/v1_1/dbripdcme/image/upload", {
            method: "post",
            body: data
        }).then(res => res.json()).
            then(data => {
                console.log('Set Photo data', data.secure_url)
                setPhoto(data.secure_url)

                axios.patch('http://34.234.203.236:8080/member/profile/image', { filename: data.secure_url }, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }).then(response => {
                    console.log('image response == ', response.data.messages)
                }).catch(error => {
                    console.log('error put image', error)
                })

            }).catch(err => {
                Alert.alert("An Error Occured While Uploading")
            })
    }

    return (
        <SafeAreaView style={styles.safeArea} >
            <ScrollView>
                <View style={styles.container}>

                    <View style={{ padding: 40 / 2, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ width: 136, height: 136, borderRadius: 80, }}>

                            {
                                avatarSource ?
                                    <Image source={{ uri: photo }}
                                        style={{
                                            width: 136,
                                            height: 136,
                                            borderWidth: 2.5,
                                            borderColor: '#ffff',
                                            borderRadius: 80
                                        }} />
                                    :
                                    <Image source={{ uri: userInfo.ProfileImg?.Filename }}
                                        style={{
                                            width: 136,
                                            height: 136,
                                            borderWidth: 2.5,
                                            borderColor: '#ffff',
                                            borderRadius: 80
                                        }} />
                            }



                            {/* <Image source={{ uri: photo }} style={{ width: 100, height: 100 }} /> */}

                            <View style={{
                                position: 'absolute',
                                width: 48,
                                height: 48,
                                borderRadius: 40,
                                backgroundColor: 'rgba(67, 72, 75, 0.85)',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderColor: '#ffff',
                                borderWidth: 1,
                                left: 112 - (40 / 2),
                                top: 112 - (40 / 2),
                            }}>
                                <TouchableOpacity onPress={() => {
                                    selectPhotoTapped()
                                }}>
                                    <Image source={require('../../assets/edit/camera.png')}
                                        style={{ width: 20, height: 20 }} />
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>

                    {/* RegisterForm */}
                    <EditForm />

                </View>
            </ScrollView>
        </SafeAreaView >
    )
}