import React, { useState } from 'react';
import { View } from 'react-native';
import { TextInput, Button, Text } from 'react-native-paper';
//css style
import styles from './Styles';
//redux
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

export default EditForm = () => {
    const navigation = useNavigation()
    const dispatch = useDispatch()
    
    const token = useSelector(state => state.auth)
    const userInfo = useSelector(state => state.user)

    const [username, setUsername] = useState(userInfo.username)
    const [email, setEmail] = useState(userInfo.email)

    const editProfile = () => {
        console.log('username : ', username)
        console.log('email : ', email)

        axios.patch('http://34.234.203.236:8080/member/profile', { "username": username, "email": email }, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(response => {
            dispatch({ type: 'GET_USER_PROFILE', payload: response.data })
            navigation.navigate('Optional')
        }).catch(error => {
            console.log('edit error', error)
        })
    }

    return (
        <View style={styles.formcontainer}>
            <View style={styles.textInputWrapper} >
                <Text style={styles.inputTitle}>Username </Text>
                <TextInput
                    mode='flat'
                    style={styles.textInput}
                    theme={{
                        colors: {
                            underlineColor: 'transparent',
                            text: '#ffff',
                        }
                    }}
                    value={username}
                    onChangeText={username => setUsername(username)}
                />
            </View>

            <View style={styles.textInputWrapper}>
                <Text style={styles.inputTitle}>Email </Text>
                <TextInput
                    mode='flat'
                    style={styles.textInput}
                    theme={{
                        colors: {
                            underlineColor: 'transparent',
                            text: '#ffff',
                        }
                    }}
                    value={email}
                    onChangeText={email => setEmail(email)}
                />
            </View>

            <Button
                style={styles.submitBotton}
                labelStyle={{ fontFamily: 'Poppins-Medium', color: '#ffff' }}
                mode='contained'
                uppercase={false}
                onPress={() => {
                    editProfile()
                }}>
                Save
            </Button>
        </View>


    )
}