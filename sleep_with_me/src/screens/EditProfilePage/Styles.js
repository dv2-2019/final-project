import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#0F2D44'
    },
    container: {
        flex: 1,
    },
    profileImage: {
        width: 136,
        height: 136,
        borderWidth: 2,
        borderColor: '#ffff',
        borderRadius: 80
    },
    userAccount: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0E2639',
        paddingVertical: 32,
    },
    //register form
    textInput: {
        backgroundColor: '#0F2D44',
        width: 312,
        fontSize: 14,
        height: 52
    },
    submitBotton: {
        width: 174,
        elevation: 5,
        padding: 6,
        elevation: 0,
        borderRadius: 28,
        marginTop: 24,
        height: 48
    },
    signin: {
        color: '#e64c42'
    },
    inputTitle: {
        color: '#a1a1a1',
        fontSize: 13
    },
    formcontainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 2,
        backgroundColor: '#0F2D44',
       paddingVertical: 24
    },
    textInputWrapper: {
        flexDirection: 'column',
        marginVertical: 8
    }

})
