import React, { useState, useRef, useCallback } from 'react';
import { View, Alert } from 'react-native';
import { TextInput, Button, HelperText, Text } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native'
import { useDispatch } from 'react-redux'
import * as yup from 'yup'
import { Formik } from 'formik'
//css style
import styles from './Styles';
import axios from 'axios';

export default RegisterForm = () => {
    const dispatch = useDispatch()
    const [isLoadingRegister, setIsLoadingRegister] = useState(false)

    const emailInput = useRef()
    const passwordInput = useRef()
    const passwordConfirmInput = useRef()

    return (
        <Formik
            initialValues={{
                username: '',
                email: '',
                password: '',
                passwordConfirm: ''

            }}
            //register form
            onSubmit={values => {
                // console.log('Register ', { username: values.username, email: values.email, password: values.password })
                axios.post('http://34.234.203.236:8080/signup',
                    { username: values.username, email: values.email, password: values.password })
                    .then(res => {
                        console.log('Register ', res.data)

                        axios.post('http://34.234.203.236:8080/login', { username: res.data.username, password: res.data.password })
                            .then(response => {

                                axios.post('http://34.234.203.236:8080/member/profile/image', {
                                    filename: "https://www.lococrossfit.com/wp-content/uploads/2019/02/user-icon-300x300.png"
                                }, {
                                    headers: {
                                        Authorization: `Bearer ${response.data.token}`
                                    }
                                }).then(response => {
                                    console.log('complete update profile  = ', response)
                                }).catch(error => {
                                    console.log('error to create initial profile image =', error)
                                })
                                //set token to reducer
                                dispatch({ type: 'SET_AUTH_TOKEN', payload: response.data.token })
                            })
                            .catch(error => {
                                console.log('error to login = ', error)
                            })
                    })
                    .catch(error => {
                        console.log('error to register = ', error)
                    })
            }}
            validationSchema={yup.object().shape({
                username: yup
                    .string()
                    .required('Please, provide your name!'),
                email: yup
                    .string()
                    .email()
                    .required(),
                password: yup
                    .string()
                    .min(6)
                    .max(10, 'Password should not excced 10 chars.')
                    .required(),
                passwordConfirm: yup.string().when("password", {
                    is: val => (val && val.length > 0 ? true : false),
                    then: yup.string().oneOf(
                        [yup.ref("password")],
                        "Password need to be the same"
                    )
                })
            })}
        >
            {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
                <View >
                    <TextInput
                        mode='outlined'
                        style={styles.textInput}
                        inlineImageLeft='user'
                        theme={{
                            roundness: 28,
                            colors: {
                                placeholder: '#A6A6A6',
                                underlineColor: 'transparent',
                                text: '#ffff',
                            }
                        }}
                        value={values.username}
                        onChangeText={handleChange('username')}
                        onBlur={() => setFieldTouched('username')}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => emailInput.current.focus()}
                        placeholder='Username'
                    />
                    {touched.username && errors.username &&
                        <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.username}</Text>
                    }

                    <TextInput
                        ref={emailInput}
                        mode='outlined'
                        style={styles.textInput}
                        theme={{
                            roundness: 28,
                            colors: {
                                placeholder: '#A6A6A6',
                                underlineColor: 'transparent',
                                text: '#ffff',
                            }
                        }}
                        value={values.email}
                        onChangeText={handleChange('email')}
                        onBlur={() => setFieldTouched('email')}
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => passwordInput.current.focus()}
                        placeholder='Email'
                    />
                    {touched.email && errors.email &&
                        <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.email}</Text>
                    }

                    <TextInput
                        ref={passwordInput}
                        mode='outlined'
                        style={styles.textInput}
                        theme={{
                            roundness: 28,
                            colors: {
                                placeholder: '#A6A6A6',
                                underlineColor: 'transparent',
                                text: '#ffff',
                            }
                        }}
                        value={values.password}
                        onChangeText={handleChange('password')}
                        onBlur={() => setFieldTouched('password')}
                        textContentType="password"
                        autoCapitalize="none"
                        returnKeyType="next"
                        onSubmitEditing={() => passwordConfirmInput.current.focus()}
                        placeholder='Password'
                        keyboardType="numeric"
                        secureTextEntry
                    />
                    {touched.password && errors.password &&
                        <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.password}</Text>
                    }
                    <TextInput
                        ref={passwordConfirmInput}
                        mode='outlined'
                        style={styles.textInput}
                        theme={{
                            roundness: 28,
                            colors: {
                                placeholder: '#A6A6A6',
                                underlineColor: 'transparent',
                                text: '#ffff',
                            }
                        }}
                        value={values.passwordConfirm}
                        onChangeText={handleChange('passwordConfirm')}
                        onBlur={() => setFieldTouched('passwordConfirm')}
                        textContentType="password"
                        keyboardType="numeric"
                        autoCapitalize="none"
                        placeholder='Password Confirm'
                        secureTextEntry
                    />
                    {touched.passwordConfirm && errors.passwordConfirm && values.password != values.passwordConfirm &&
                        <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.passwordConfirm}</Text>
                    }
                    <View style={{ alignItems: 'center', marginVertical: 24 }}>
                        <Button
                            style={styles.submitBotton}
                            labelStyle={{ fontFamily: 'Poppins-Medium', color: '#ffff' }}
                            mode='contained'
                            loading={isLoadingRegister}
                            disabled={!isValid}
                            onPress={() => {
                                handleSubmit()
                            }}>
                            Create
                        </Button>
                    </View>
                </View>
            )}
        </Formik>

    )
}