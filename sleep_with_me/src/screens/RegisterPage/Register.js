import React, { useState } from 'react';
import { View, ScrollView, ImageBackground } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import { Text } from 'react-native-paper';
import RegisterForm from './RegisterForm';

//css style
import styles from './Styles';
export default Register = () => {
    return (
        <SafeAreaView style={styles.safeArea} >
            <ScrollView>
                <View style={styles.container}>
                    {/* add image profile */}
                    <View style={styles.header}>
                        <Text style={styles.headerText}>Let’s get started!</Text>
                        <Text style={styles.subHeader}>Create an account to get all features.</Text>
                    </View>

                    {/* RegisterForm */}
                    <RegisterForm />

                </View>
            </ScrollView>
        </SafeAreaView >
    )
}