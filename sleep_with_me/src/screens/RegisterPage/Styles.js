import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#0F2D44'
    },
    imageBg: {
        flex: 1,
    },
    container: {
        flex: 1,
        paddingVertical: 16,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    header: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingBottom: 24,
    },
    headerText: {
        fontSize: 22,
        color: '#F7BB47',
        fontFamily: 'Poppins-Medium',
    },
    subHeader: {
        color: '#dfdfdf',
    },
    //register form
    textInput: {
        backgroundColor: '#0E2639',
        width: 324,
        marginVertical: 4,
        fontSize: 14,
    },
    submitBotton: {
        width: 174,
        elevation: 5,
        padding: 6,
        elevation: 0,
        borderRadius: 28,
        height: 52
    },
    askAccount: {
        flexDirection: 'row',
        marginTop: 15,
        paddingBottom: 50
    },
    signin: {
        color: '#e64c42'
    },

})
