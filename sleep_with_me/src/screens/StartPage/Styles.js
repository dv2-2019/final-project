import { StyleSheet } from 'react-native';
export default styles = StyleSheet.create({
    image: {
        width: 240,
        height: 224,
    },
    imageContainer: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 1
    },
    title: {
        fontFamily: 'Poppins-Medium',
        fontSize: 20,
        color: '#ffff'
    },
    description: {
        justifyContent: 'flex-start',
        alignItems: 'center', flex: 1,
        paddingTop: 32
    },
    slide: {
        flex: 1,
        backgroundColor: '#0F2D44'
    },
    text: {
        fontSize: 14,
        color: '#a6a6a6',
        paddingVertical: 8,
        marginHorizontal: 40
    },
    startBotton: {
        width: 208,
        elevation: 2,
        padding: 4,
        borderRadius: 28,
        height: 50,
    },
    buttonContainer: {
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 8
    }
});