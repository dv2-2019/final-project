import React from 'react';
import { View, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Text, Button } from 'react-native-paper';
//css styles
import styles from './Styles';
import Login from '../LoginPage/Login';

const slides = [
    {
        key: 1,
        title: 'Sleep Monitor',
        text: 'By tracking the time you’re inactive, record when you fall asleep with stopwatch timer at night.',
        image: require('../../assets/start/icon_start1.png'),
        backgroundColor: '#59b2ab',
    },
    {
        key: 2,
        title: 'Sleep Statistics',
        text: 'Calculate sleep time and awake time and showing as a statistic graph.',
        image: require('../../assets/start/icon_start2.png'),
        backgroundColor: '#febe29',
    },
    {
        key: 3,
        title: 'Healthcare',
        text: 'Report your sleep health and anticipate determinant of health and well-being.',
        image: require('../../assets/start/icon_start3.png'),
        backgroundColor: '#22bcb5',
    },
    {
        key: 4,
        title: 'Share tooltips',
        text: 'The way to share technique or tooltips to others and improve your sleep behavior.',
        image: require('../../assets/start/icon_start4.png'),
        backgroundColor: '#22bcb5',
    }
]

export default class SlideStart extends React.Component {

    state = {
        showRealApp: false
    }

    _renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                <View style={styles.imageContainer}>
                    <Image source={item.image} style={styles.image} />
                </View>
                <View style={styles.description}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.text}>{item.text}</Text>
                </View>
            </View>
        )
    }
    _onDone = () => {
        this.setState({ showRealApp: true })
    }

    _renderDoneButton = () => {
        return (
            <View style={styles.buttonContainer}>
                <Button
                    style={styles.startBotton}
                    labelStyle={{ color: '#ffff', fontSize: 14 }}
                    mode='contained'
                    uppercase={false}
                    onPress={() => {
                        this.props.navigation.navigate('Login')
                    }}>
                    Let’s get start
                </Button>
            </View>
        )
    }

    render() {
        if (this.state.showRealApp) {
            return <App />
        } else {
            return <AppIntroSlider
                renderItem={this._renderItem}
                data={slides}
                onDone={this._onDone}
                showNextButton={false}
                bottomButton
                renderDoneButton={this._renderDoneButton}
                activeDotStyle={{ backgroundColor: '#46C7F0' }}
            />
        }
    }
}
