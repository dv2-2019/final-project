import { AppState, PushNotificationIOS } from 'react-native';
import PushNotification from 'react-native-push-notification';

const _handleAppStateChange = nextAppState => {
    if (nextAppState === 'active') {
        _registerLocalNotification();
    }
};

const _registerLocalNotification = () => {
    PushNotification.setApplicationIconBadgeNumber(0);
    PushNotification.cancelAllLocalNotifications();

    const message = 'this is for test killed app notification';

    //This is only for testing purpose, triggers a notification one minute after current time
    const date = new Date(Date.now() + 15 * 1000);

    PushNotification.localNotificationSchedule({
        /* Android Only Properties */
        vibrate: true,
        vibration: 300,
        priority: 'high',
        visibility: 'public',
        importance: 'high',

        /* iOS and Android properties */
        message, // (required)
        playSound: false,
        number: 1,
        actions: '["OK"]',

        // Production timestamp
        repeatType: 'day',
        date: date
    });
};

useEffect(() => {

    Notifications.register();

    return (() => {
        Notifications.unregister();
    })
});
export default {
    register: async () => {
        PushNotification.configure({
            onNotification: function (notification) {
                notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
            popInitialNotification: true,
        });

        _registerLocalNotification();

        AppState.addEventListener('change', _handleAppStateChange);
    },
    unregister: () => {
        AppState.removeEventListener('change', _handleAppStateChange);
    },
};