import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { List } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//icon
import Icon from 'react-native-vector-icons/AntDesign';
//css style
import styles from './Styles';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';

export default OptionList = () => {
    const dispatch = useDispatch()

    const navigation = useNavigation()
    const token = useSelector(state => state.auth)

    //  dispatch({ type: 'SET_AUTH_TOKEN', payload: null })
    const logout = () => {
        axios.post('http://34.234.203.236:8080/logout', {}, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(() => {
                dispatch({ type: 'SET_AUTH_TOKEN', payload: null })
            })
            .catch((err) => {
                console.log('logout error = ', err)
            })
    }

    return (
        <View style={styles.listContainer}>
            <List.Section>
                <View style={[styles.sectionContainer, styles.lineTopSection]}>
                    <View style={{ flex: 5 }}>
                        <TouchableOpacity
                            onPress={() => {
                                navigation.navigate('MyPost')
                            }}>
                            <List.Item
                                style={styles.listItem}
                                title="My Post"
                                titleStyle={{ fontSize: 14, color: '#ffff' }}
                                left={() => <Icon name="logout" style={styles.icon} size={16} />}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.forwardIcon}>
                        <Ionicons name='ios-arrow-forward' size={24} color='#46C7F0' />
                    </View>
                </View>

                <View style={styles.sectionContainer}>
                    <View style={{ flex: 5 }}>
                        <TouchableOpacity
                            onPress={() => {
                                logout()
                            }}>
                            <List.Item
                                style={styles.listItem}
                                title="Log out"
                                titleStyle={{ fontSize: 14, color: '#ffff' }}
                                left={() => <Icon name="logout" style={styles.icon} size={16} />}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.forwardIcon}>
                        <Ionicons name='ios-arrow-forward' size={24} color='#46C7F0' />
                    </View>
                </View>
            </List.Section>
        </View>
    )

}