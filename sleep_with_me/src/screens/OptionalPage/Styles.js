import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerContainer: {
        flex: 1,
        backgroundColor: '#0E2639',
        paddingTop: 48,
        paddingLeft: 16
    },
    headerFlex1: {
        justifyContent: 'flex-start',
        flex: 1
    },
    headerText: {
        fontSize: 20,
        color: '#ffff',
        fontFamily: 'Poppins-Regular'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#0F2D44'
    },
    userImg: {
        flex: 1,
        justifyContent: 'center'
    },
    userInfo: {
        flex: 2,
        justifyContent: 'center'
    },
    editButton: {
        borderColor: '#ffff',
        borderWidth: 1,
        borderRadius: 24,
        height: 36,
        width: 102,
        paddingBottom: 4
    },
    profileImg: {
        borderColor: '#ffff',
        borderWidth: 1,
        width: 80,
        height: 80,
        borderRadius: 80
    },
    userAccount: {
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingBottom: 24,
        backgroundColor: '#0E2639',
        borderBottomWidth: 1,
        borderColor: '#19374D',
    },
    nameText: {
        fontSize: 18,
        paddingTop: 18,
        fontFamily: 'Poppins-Regular',
        color: '#ffff'
    },
    usernameText: {
        fontSize: 12,
        fontFamily: 'Poppins-Regular',
        color: '#d6d6d6',
    },
    listItem: {
        height: 46,
        justifyContent: 'center',
        padding: 8,
        color: '#ffff'
    },
    listContainer: {
        flex: 1,
    },
    icon: {
        padding: 6,
        color: '#ffff'
    },
    subHeader: {
        color: '#efefef'
    },
    sectionContainer: {
        flexDirection: 'row',
        paddingVertical: 8,
        borderBottomWidth: 1,
        borderColor: '#19374D',
        paddingLeft: 8,
        backgroundColor: '#0E2639'
    },
    forwardIcon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    lineTopSection: {
        borderTopWidth: 1,
        borderColor: '#19374D',
    }

})
