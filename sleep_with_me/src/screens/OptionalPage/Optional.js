import React, { useEffect, useState } from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import { Avatar, Button } from 'react-native-paper';
import OptionList from './OptionalList';
import SafeAreaView from 'react-native-safe-area-view';

//css style 
import styles from './Styles';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';

export default Optional = () => {
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const token = useSelector(state => state.auth)
    const [userInfo, setUserInfo] = useState({})

    const fetchUserProfile = () => {
        axios.get('http://34.234.203.236:8080/member/profile', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(response => {
            dispatch({ type: 'GET_USER_PROFILE', payload: response.data })
            setUserInfo(response.data)
            // console.log('user info ', response.data)
        }).catch(error => {
            console.log('error get profile user = ', error)
        })
    }

    useFocusEffect(
        React.useCallback(() => {
            fetchUserProfile()
        }, [])
    )

    return (
        <SafeAreaView style={styles.safeArea} >
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.headerContainer}>
                        <View style={styles.headerFlex1}>
                            <Text style={styles.headerText}>Profile</Text>
                        </View>
                    </View>

                    <View style={styles.userAccount}>
                        <View style={styles.userImg}>
                            <Image source={{ uri: userInfo.ProfileImg?.Filename }} style={styles.profileImg} />
                        </View>
                        <View style={styles.userInfo}>
                            <View>
                                <Text style={styles.nameText}>{userInfo.username}</Text>
                                <Text style={styles.usernameText}>{userInfo.email}</Text>
                            </View>
                            <View style={{ paddingVertical: 8 }}>
                                <Button
                                    uppercase={false}
                                    style={styles.editButton}
                                    labelStyle={{ color: '#ffff', fontSize: 11, fontFamily: 'Poppins-Regular' }}
                                    onPress={() => { navigation.navigate('EditProfile') }}>
                                    Edit Profile
                            </Button>
                            </View>
                        </View>
                    </View>

                    {/* OptionList */}
                    <OptionList />

                </View>
            </ScrollView>
        </SafeAreaView>
    )
}