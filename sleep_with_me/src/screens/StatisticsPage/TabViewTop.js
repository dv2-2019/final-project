import React, { useState } from 'react';
import { Dimensions } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
//screens 
import StatisticsDay from './Day/StatisticsDay';
import StatisticsWeek from './Week/StatisticsWeek';

//css style
import StatisticsMonth from './Month/StatisticsMonth';
import StatisticsList from './AllStatistics/StatisticsList';
const initialLayout = { width: Dimensions.get('window').width };

export default TabViewTop = () => {
    const [index, setIndex] = useState(0)
    const [routes] = useState([
        { key: 'first', title: 'Day' },
        { key: 'second', title: 'Week' },
        { key: 'third', title: 'Month' },
        { key: 'fourth', title: 'All' },
    ]);

    const renderScene = SceneMap({
        first: StatisticsDay,
        second: StatisticsWeek,
        third: StatisticsMonth,
        fourth: StatisticsList
    });

    const renderTabBar = props => (
        <TabBar
            {...props}
            getLabelText={({ route }) => route.title}
            indicatorStyle={{ backgroundColor: '#46C7F0' }}
            style={{ backgroundColor: 'rgba(15, 45, 68, 0.95)', elevation: 1, paddingTop: 8 }}
        />
    )
    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            renderTabBar={renderTabBar}
        />
    )
}