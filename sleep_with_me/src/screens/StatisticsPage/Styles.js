import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    safeArea: {
        flex: 1,
    },
    imageBg: {
        flex: 1,
    },
    container: {
        flex: 1,
        // paddingBottom: 14
    },
    headerContainer: {
        flex: 1,
        backgroundColor: 'rgba(15, 45, 68, 0.95)',
        paddingTop: 48,
        paddingLeft: 16
    },
    headerFlex1: {
        justifyContent: 'center',
        flex: 1
    },
    headerText: {
        fontSize: 20,
        color: '#ffff',
        fontFamily: 'Poppins-Regular'
    },
})