import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    listContainer: {
        marginHorizontal: 16,
    },
    period: {
        borderRadius: 8,
        backgroundColor: '#d7d7d7',
        marginVertical: 16,
        marginHorizontal: 8,
        padding: 8
    },
    periodActive: {
        backgroundColor: '#0F2D44',
        borderRadius: 8,
        marginVertical: 16,
        marginHorizontal: 8,
        padding: 8,
        color: '#ffff'
    },
    listItem: {
        backgroundColor: 'rgba(48, 84, 112, 0.95)',
        borderRadius: 8,
        marginVertical: 4,
        padding: 16,
        elevation: 1
    },
    listIcon: {
        width: 40,
        height: 40,
        padding: 16
    }

})