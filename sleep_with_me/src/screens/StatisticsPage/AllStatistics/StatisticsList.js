import React from 'react';
import { View, FlatList, Image } from 'react-native';
import { List, Card } from 'react-native-paper';

//redux
import { useSelector } from 'react-redux';
//css styles
import styles from '../AllStatistics/Styles';
import Axios from 'axios';

export default StatisticsList = () => {
    const statGroup = useSelector(stat => stat.statistics.statMonth)

    const calSleepTime = (startTime) => {
        console.log('start time === ', startTime)
        let startDate = new Date(startTime * 1000);
        let startHours = startDate.getHours();
        let startMinutes = "0" + startDate.getMinutes();
        let formattedStartTime = startHours + ':' + startMinutes.substr(-2)
        return formattedStartTime
    }

    const calAwakeTime = (stopTime) => {
        let startDate = new Date(stopTime * 1000);
        let startHours = startDate.getHours();
        let startMinutes = "0" + startDate.getMinutes();
        let formattedStartTime = startHours + ':' + startMinutes.substr(-2)
        return formattedStartTime
    }

    const avgSleepTime = (lapTime) => {
        //average time 
        var minutes = ("" + (Math.floor(lapTime / 60000) % 60)).slice(-2)
        var hours = ("" + Math.floor(lapTime / 3600000)).slice(-2)
        return hours + "h" + minutes + "m"
    }

    const percentage = (lapTime) => {
        //percentage
        var percent = Math.round((lapTime * 100) / 28800000)
        return percent
    }

    return (
        <View style={styles.listContainer}>
            <List.Section title="History" titleStyle={{ color: '#ffff' }}>
                <FlatList
                    data={statGroup}
                    renderItem={({ item }) => (
                        <List.Item
                            style={styles.listItem}
                            titleStyle={{ fontSize: 14, color: '#ffff', fontFamily: 'Poppins-Medium' }}
                            descriptionStyle={{ fontSize: 12, color: '#c2c2c2' }}
                            title={"Sleep " + calSleepTime(item.startTime) + " - " + calAwakeTime(item.stopTime)}
                            description={"Date : " + item.startDate + " - " + item.stopDate + `\n` + "Avg " + avgSleepTime(item.lapTime) + " : " + percentage(item.lapTime) + "%/8hrs"}
                            left={() => <Image source={require('../../../assets/stat/day.png')} style={styles.listIcon} />}
                        />
                    )}
                    keyExtractor={(index) => index + ''}
                />
            </List.Section>
        </View>
    )
}