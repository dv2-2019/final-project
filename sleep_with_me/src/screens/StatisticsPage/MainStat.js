import React, { useState } from 'react';
import { View, Text, ImageBackground, ScrollView } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
//css style
import styles from './Styles';
import TabViewTop from './TabViewTop';

export default MainStat = () => {

    return (
        <SafeAreaView style={styles.safeArea} >
            <ImageBackground source={require('../../assets/bg_sun_mountain_dark.png')} style={styles.imageBg}>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.headerContainer}>
                            <View style={styles.headerFlex1}>
                                <Text style={styles.headerText}>Sleep Tracking</Text>
                            </View>
                        </View>

                        <View>
                            <TabViewTop />
                        </View>

                    </View>
                </ScrollView>
            </ImageBackground>
        </SafeAreaView>

    );
}