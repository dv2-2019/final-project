import React from 'react';
import { View } from 'react-native';
import { Card, Text } from 'react-native-paper';
import ProgressCircle from 'react-native-progress-circle'
//css  style
import styles from './css/QualityStyles';
import { useSelector } from 'react-redux';

export default QualityWeek = () => {
    const statWeek = useSelector(state => state.statistics.statWeek)

    var averageLapTime = statWeek.reduce((total, value) => total + value.lapTime, 0) / statWeek.length
    // console.log('laptime', averageLapTime)
    var minutes = ("" + (Math.floor(averageLapTime / 60000) % 60)).slice(-2)
    var hours = ("" + Math.floor(averageLapTime / 3600000)).slice(-2)
    var seconds = ("" + (Math.floor(averageLapTime / 1000) % 60)).slice(-2)

    //percentage
    var percent = Math.round((averageLapTime * 100) / 28800000)

    return (
        <View style={styles.qualityContainer}>
            <View style={styles.headerContent}>
                <Text style={styles.contentText}>Sleep Quality  </Text>
            </View>

            <View style={styles.rowContainer}>
                <View style={styles.section}>
                    {/* <Card style={styles.percentCard}> */}
                    <View style={styles.percentText}>
                        <ProgressCircle
                            percent={percent}
                            radius={48}
                            borderWidth={12}
                            color="#46C7F0"
                            shadowColor="#A8A8A8"
                            bgColor="#0D1F2D"
                        >
                            <Text style={{ fontSize: 16, fontFamily: 'Poppins-Regular', color: '#ffff' }}>{percent + '%'}</Text>
                        </ProgressCircle>
                    </View>
                    <View style={styles.avgText2}>
                        <Text style={styles.textWhite}>Sleep time 8 hrs/night</Text>
                    </View>
                    {/* </Card> */}
                </View>

                <View style={styles.section}>
                    <View style={styles.display}>
                        <Text style={styles.avgTime}>{hours}h {minutes}m</Text>
                        <Text style={styles.textWhite} >Average</Text>
                    </View>
                </View>

            </View>
        </View>
    )
}