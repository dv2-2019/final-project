import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { BarIndicator } from 'react-native-indicators';
//redux
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
//css style
import styles from './css/GraphStyles';
//screen
import GraphWeek from './GraphWeek';
import QualityWeek from './QualityWeek';
import ResultWeek from './ResultWeek';

export default StatisticsWeek = () => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true)

    const token = useSelector(state => state.auth)

    console.log('token for statistics  ', token)
    const iniData = [{
        id: 1,
        lapTime: 1,
        startDate: "no data",
        startTime: 1,
        stopDate: "no data",
        stopTime: 1
    },
    {
        id: 2,
        lapTime: 1,
        startDate: "no data",
        startTime: 10,
        stopDate: "no data",
        stopTime: 5
    },
    {
        id: 3,
        lapTime: 1,
        startDate: "no data",
        startTime: 8,
        stopDate: "no data",
        stopTime: 2
    },
    {
        id: 4,
        lapTime: 1,
        startDate: "no data",
        startTime: 9,
        stopDate: "no data",
        stopTime: 4
    }]

    const fetchStatWeekData = () => {
        axios.get('http://34.234.203.236:8080/member/statistics', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(response => {
            if (response.data.length <= 0) {
                console.log("aaaaaaaaaa22222");
                dispatch({ type: 'GET_STATWEEK', payload: iniData })
            } else {
                console.log("aaaaaaaaaa33333");
                var temp = response.data.filter((data) => {
                    return data.id <= 7
                })
                // console.log('res data week', temp)
                dispatch({ type: 'GET_STATWEEK', payload: temp })
            }
            setLoading(false)
        })
            .catch(error => {
                setLoading(false)
                if (error.response) {
                    console.log('error setch stat week : ', error)
                }
                else {
                    alert('Cannot connect internet')
                }
            })
    }

    useEffect(() => {
        fetchStatWeekData()
    }, [])

    //set loading page
    if (loading) {
        return <View style={styles.loading}>
            <BarIndicator color='white' count={5} />
        </View>
    }

    return (
        <View>
            {/* graph history */}
            <GraphWeek />

            {/* sleep quality */}
            <QualityWeek />

            {/* sleep result */}
            <ResultWeek />
        </View>
    )
}