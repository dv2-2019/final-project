import React, { useEffect, useState } from 'react';
import { View, Image } from 'react-native';
import { Text } from 'react-native-paper';
import { BallIndicator } from 'react-native-indicators';
//redux
import { useSelector } from 'react-redux';
//css style
import styles from './css/ResultStyles';
import axios from 'axios';

export default ResultDay = () => {
    const statDay = useSelector(state => state.statistics.statDay)
    const [loading, setLoading] = useState(true)
    const [resultDay, setResultDay] = useState([])

    //cal sleep laptime unix timestamp
    var minutes = ("0" + (Math.floor(statDay.lapTime / 60000) % 60)).slice(-2)
    var hours = ("0" + Math.floor(statDay.lapTime / 3600000)).slice(-2)
    var totalSleep = parseFloat(hours + '.' + minutes)

    //cal time
    let startDate = new Date(statDay.startTime * 1000);
    let startHours = startDate.getHours();
    let startMinutes = "0" + startDate.getMinutes();
    let formattedStartTime = parseFloat(startHours + '.' + startMinutes.substr(-2));

    const fetchResultSleep = () => {
        axios.get('https://5e92918abbff810016968fbe.mockapi.io/api/v1/sleepResult')
            .then(response => {
                // console.log('result =', response.data)

                if (formattedStartTime >= 20.00 && formattedStartTime <= 23.00) {//23
                    if (totalSleep >= 7 && totalSleep < 10) {
                        setResultDay(response.data[0])
                    } else if (totalSleep >= 10) {
                        setResultDay(response.data[2])
                    } else if (totalSleep < 7) {
                        setResultDay(response.data[3])
                    }
                }
                else if (formattedStartTime < 20.00 && formattedStartTime > 23.00) {
                    if (totalSleep >= 7 && totalSleep < 10) {
                        setResultDay(response.data[1])
                    } else if (totalSleep >= 10) {
                        setResultDay(response.data[2])
                    } else if (totalSleep < 7) {
                        setResultDay(response.data[3])
                    }
                }
                setLoading(false)
            })
            .catch(error => {
                console.log('error', error.response.data.messages)
            })
    }

    useEffect(() => {
        fetchResultSleep()
    }, [])

    if (loading) {
        return <View style={styles.loading}>
            <BallIndicator color='white' count={8} />
        </View>
    }

    return (
        <View style={styles.resultContainer}>
            <View style={styles.headerContent}>
                <Text style={styles.contentText}>Sleep Cycle : </Text>
            </View>

            <View style={styles.result}>
                <View style={styles.resultSection}>
                    <Text style={styles.resultDes}>         {resultDay.result}</Text>
                </View>
                {
                    totalSleep > 7 ?
                        <View style={styles.resultImgContainer}>
                            <Image source={require('../../../assets/stat/sleepenough.png')} style={styles.resultImg} />
                        </View>
                        :
                        <View style={styles.resultImgContainer}>
                            <Image source={require('../../../assets/stat/sleepless.png')} style={styles.resultImg} />
                        </View>
                }

            </View>

        </View>
    )
}