import React from 'react';
import { View, Text } from 'react-native';
import { LineChart, } from "react-native-chart-kit";
//redux
import { useSelector } from 'react-redux';
//css style
import styles from './css/GraphStyles';

export default GraphDay = () => {
    const statDay = useSelector(state => state.statistics.statDay)
    // console.log('get stat day', statDay)

    //cal sleep unix timestamp
    let startDate = new Date(statDay.startTime * 1000);
    let startHours = startDate.getHours();
    let startMinutes = "0" + startDate.getMinutes();
    let formattedStartTime = parseFloat(startHours + '.' + startMinutes.substr(-2));
    //cal awakeunix timestamp
    let stopDate = new Date(statDay.stopTime * 1000);
    let stopHours = stopDate.getHours();
    let stopMinutes = "0" + stopDate.getMinutes();
    let formattedStopTime = parseFloat(stopHours + '.' + stopMinutes.substr(-2));

    //graph data 
    const sleepData = {
        labels: [statDay.startDate],
        datasets: [
            {
                data: [formattedStartTime],
                color: (opacity = 1) => `rgba(16, 177, 110, ${opacity})`,
                strokeWidth: 3
            }
        ],
    }
    const awakeData = {
        labels: [statDay.stopDate],
        datasets: [
            {
                data: [formattedStopTime],
                color: (opacity = 1) => `rgba(16, 177, 110, ${opacity})`,
                strokeWidth: 3
            }
        ],
    }
    const chartConfig = {
        backgroundGradientFrom: 'rgba(25, 55, 77, 0.97)',
        backgroundGradientFromOpacity: 0,
        backgroundGradientToOpacity: 0.5,
        color: (opacity = 1) => `rgba(239, 239, 239, ${opacity})`,
        strokeWidth: 2, // optional, default 3
        barPercentage: 0.5,
        propsForDots: {
            r: "5",
            strokeWidth: "2",
        },
    }

    return (
        <View style={styles.graphContainer}>
            <View>
                <View style={styles.headerContent}>
                    <Text style={styles.contentText}>Went to bed : </Text>
                </View>
                <View style={styles.graphView}>
                    <LineChart
                        data={sleepData}
                        width={352}
                        height={254}
                        verticalLabelRotation={15}
                        chartConfig={chartConfig}
                        bezier
                        fromZero={true}
                    />
                </View>
            </View>

            <View>
                <View style={styles.headerWrapper}>
                    <Text style={styles.contentText}>Awake : </Text>
                </View>
                <View style={styles.graphView}>
                    <LineChart
                        data={awakeData}
                        width={352}
                        height={224}
                        verticalLabelRotation={15}
                        chartConfig={chartConfig}
                        bezier
                        fromZero={true}
                    />
                </View>
            </View>
        </View>
    )
}