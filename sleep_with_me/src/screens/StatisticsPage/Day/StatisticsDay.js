import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { BarIndicator } from 'react-native-indicators';
//redux
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
//css style
import styles from './css/GraphStyles';
//screen
import GraphDay from './GraphDay';
import QualityDay from './QualityDay';
import ResultDay from './ResultDay';

export default StatisticsDay = () => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true)

    const token = useSelector(state => state.auth)
    console.log('token for statistics  ', token)

    const iniData = [{
        id: 1,
        lapTime: 1,
        startDate: "no data",
        startTime: 1,
        stopDate: "no data",
        stopTime: 1
    },
    {
        id: 2,
        lapTime: 1,
        startDate: "no data",
        startTime: 10,
        stopDate: "no data",
        stopTime: 5
    },
    {
        id: 3,
        lapTime: 1,
        startDate: "no data",
        startTime: 8,
        stopDate: "no data",
        stopTime: 2
    },
    {
        id: 4,
        lapTime: 1,
        startDate: "no data",
        startTime: 9,
        stopDate: "no data",
        stopTime: 4
    }]

    const fetchStatDayData = () => {
        axios.get('http://34.234.203.236:8080/member/statistics', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(response => {
            // console.log('res data', response.data[0])
            if (response.data.length <= 0) {
                console.log("dddddddddddd2222s");
                dispatch({ type: 'GET_STATDAY', payload: iniData[0] })
            } else {
                console.log("ressssssss33333");
                dispatch({ type: 'GET_STATDAY', payload: response.data[0] })
            }
            setLoading(false)
        }).catch(error => {
            console.log("errrrr12222");
            if (error.response) {
                console.log('error fetch stat day : ', error)
            }
            else {
                alert('Cannot connect internet')
            }
        })
    }

    useEffect(() => {
        fetchStatDayData()
    }, [])

    //set loading page
    if (loading) {
        return <View style={styles.loading}>
            <BarIndicator color='white' count={5} />
        </View>
    }

    return (
        <View>
            {/* graph history */}
            <GraphDay />

            {/* sleep quality */}
            <QualityDay />

            {/* sleep result */}
            <ResultDay />
        </View>
    )
}