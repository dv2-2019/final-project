import React from 'react';
import { View } from 'react-native';
import { Card, Text } from 'react-native-paper';
import ProgressCircle from 'react-native-progress-circle'
//redux
import { useSelector } from 'react-redux';
//css  style
import styles from './css/QualityStyles';

export default QualityMonth = () => {
    const statMonth = useSelector(state => state.statistics.statMonth)

    var averageLapTime = statMonth.reduce((total, value) => total + value.lapTime, 0) / statMonth.length
    // console.log('laptime month', averageLapTime)
    var minutes = ("" + (Math.floor(averageLapTime / 60000) % 60)).slice(-2)
    var hours = ("" + Math.floor(averageLapTime / 3600000)).slice(-2)
    var seconds = ("" + (Math.floor(averageLapTime / 1000) % 60)).slice(-2)

    //percentage
    var percent = Math.round((averageLapTime * 100) / 28800000)

    return (
        <View style={styles.qualityContainer}>
            <View style={styles.headerContent}>
                <Text style={styles.contentText}>Sleep Quality  </Text>
            </View>

            <View style={styles.rowContainer}>
                <View style={styles.section}>
                    {/* <Card style={styles.percentCard}> */}
                    <View style={styles.percentText}>
                        <ProgressCircle
                            percent={percent}
                            radius={48}
                            borderWidth={12}
                            color="#33A1C3"
                            shadowColor="#A8A8A8"
                            bgColor="#0D1F2D"
                        >
                            <Text style={{ fontSize: 16, fontFamily: 'Poppins-Regular',color:'#ffff' }}>{percent + '%'}</Text>
                        </ProgressCircle>
                    </View>
                    <View style={styles.avgText2}>
                        <Text style={styles.textWhite}>Sleep time 8 hrs/night</Text>
                    </View>
                    {/* </Card> */}
                </View>

                <View style={styles.section}>
                    <View style={styles.display}>
                        <Text style={styles.avgTime}>{hours}h {minutes}m</Text>
                        <Text style={styles.textWhite} >Average</Text>
                    </View>
                </View>

            </View>
        </View>
    )
}