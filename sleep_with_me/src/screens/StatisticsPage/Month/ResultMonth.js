import React, { useEffect, useState } from 'react';
import { View, Image } from 'react-native';
import { Text } from 'react-native-paper';
import { BallIndicator } from 'react-native-indicators';
//redux
import { useSelector } from 'react-redux';
//css style
import styles from './css/ResultStyles';
import axios from 'axios';

export default ResultMonth = () => {
    const statMonth = useSelector(state => state.statistics.statMonth)
    const [loading, setLoading] = useState(true)
    const [resultMonth, setResultMonth] = useState([])

    //cal total sleep laptime 
    var averageLapTime = statMonth.reduce((total, value) => total + value.lapTime, 0) / statMonth.length
    var minutes = ("0" + (Math.floor(averageLapTime / 60000) % 60)).slice(-2)
    var hours = ("0" + Math.floor(averageLapTime / 3600000)).slice(-2)
    var totalSleep = parseFloat(hours + '.' + minutes)

    //cal total start sleep
    const sleepYLabel = statMonth.map((dataYS) => {
        let startDate = new Date(dataYS.startTime * 1000);
        let startHours = startDate.getHours();
        let startMinutes = "0" + startDate.getMinutes();
        let formattedStartTime = parseFloat(startHours + '.' + startMinutes.substr(-2));
        return formattedStartTime
    })
    const avgStartSleep = Math.round(sleepYLabel.reduce((total, value) => total + value, 0) / sleepYLabel.length)

    const fetchResultSleepWeek = () => {
        axios.get('https://5e92918abbff810016968fbe.mockapi.io/api/v1/sleepResult')
            .then(response => {
                // console.log('response', response.data)

                if (avgStartSleep >= 20.00 && avgStartSleep <= 23.00) {//23
                    if (totalSleep >= 7 && totalSleep < 10) {
                        setResultMonth(response.data[0])
                    } else if (totalSleep >= 10) {
                        setResultMonth(response.data[2])
                    } else if (totalSleep < 7) {
                        setResultMonth(response.data[3])
                    }
                }
                else if (avgStartSleep < 20.00 && avgStartSleep > 23.00) {
                    if (totalSleep >= 7 && totalSleep < 10) {
                        setResultMonth(response.data[1])
                    } else if (totalSleep >= 10) {
                        setResultMonth(response.data[2])
                    } else if (totalSleep < 7) {
                        setResultMonth(response.data[3])
                    }
                }
                setLoading(false)
            })
            .catch(error => {
                console.log('error', error.response.data.message)
            })
    }

    useEffect(() => {
        fetchResultSleepWeek()
    }, [])

    if (loading) {
        return <View style={styles.loading}>
            <BallIndicator color='white' count={8} />
        </View>
    }

    return (
        <View style={styles.resultContainer}>
            <View style={styles.headerContent}>
                <Text style={styles.contentText}>Sleep Cycle : </Text>
            </View>

            <View style={styles.result}>
                <View style={styles.resultSection}>
                    <Text style={styles.resultDes}>         {resultMonth.result}</Text>

                </View>
                {
                    totalSleep > 7 ?
                        <View style={styles.resultImgContainer}>
                            <Image source={require('../../../assets/stat/sleepenough.png')} style={styles.resultImg} />
                        </View>
                        :
                        <View style={styles.resultImgContainer}>
                            <Image source={require('../../../assets/stat/sleepless.png')} style={styles.resultImg} />
                        </View>
                }
            </View>

        </View>
    )
}