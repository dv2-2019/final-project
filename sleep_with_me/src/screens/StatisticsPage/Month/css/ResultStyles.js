import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    //result component
    resultContainer: {
        flex: 1,
        backgroundColor: 'rgba(13, 31, 45, 0.70)',
        marginHorizontal: 16,
        borderBottomLeftRadius: 18,
        borderBottomRightRadius: 18,
        marginBottom: 16
    },
    headerContent: {
        paddingTop: 16,
        paddingLeft: 16,
        flex: 1,
    },
    contentText: {
        fontSize: 14,
        fontFamily: 'Poppins-Medium',
        color: '#ffff'
    },
    result: {
        flex: 1,
        marginTop : 24
    },
    resultSection: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 36,
    },
    resultDes: {
        fontFamily: 'Poppins-Light',
        color: '#ffff',
        fontSize: 12
    },
    resultImgContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 24
    },
    resultImg: {
        width: 154,
        height: 96
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }


})