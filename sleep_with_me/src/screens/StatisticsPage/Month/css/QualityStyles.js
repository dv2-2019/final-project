import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    //quality component 
    headerContent: {
        paddingTop: 32,
        paddingLeft: 16,
        flex: 1,
    },
    contentText: {
        fontSize: 14,
        fontFamily: 'Poppins-Medium',
        color: '#ffff'
    },
    qualityContainer: {
        flex: 1,
        backgroundColor: 'rgba(13, 31, 45, 0.65)',
        marginHorizontal: 16,
    },
    textWhite: {
        color: '#afafaf',
        fontSize: 12
    },
    avgTime: {
        fontSize: 24,
        fontFamily: 'Poppins-Medium',
        color: '#ffff'
    },
    rowContainer: {
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 16
    },
    section: {
        flex: 1,
        marginHorizontal: 4
    },
    avgTimeCard: {
        height: 134,
        borderRadius: 8,
        backgroundColor: '#0F2D44',
        padding: 10,
    },
    avgtext: {
        flex: 1
    },
    avgText2: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 1,
        paddingTop: 8
    },
    display: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 1,
        paddingLeft: 8
    },
    percentCard: {
        height: 134,
        borderRadius: 8,
        backgroundColor: '#c2c5c7',
        padding: 10,
    },
    percentText: {
        flex: 2,
        alignItems: 'center',
    },
    textBlue: {
        color: '#337489',
        fontSize: 12
    }


})