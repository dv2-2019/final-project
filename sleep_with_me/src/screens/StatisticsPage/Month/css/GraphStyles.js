import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    //graph component
    graphContainer: {
        marginHorizontal: 16,
        marginTop: 16,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
    graphView: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(25, 55, 77, 0.97)',
        paddingVertical: 16,

    },
    headerContent: {
        padding: 16,
        flex: 1,
        backgroundColor: 'rgba(25, 55, 77, 0.97)',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
    },
    contentText: {
        fontSize: 14,
        fontFamily: 'Poppins-Medium',
        color: '#ffff'
    },
    headerWrapper: {
        padding: 16,
        flex: 1,
        backgroundColor: 'rgba(25, 55, 77, 0.97)',
    },
    loading: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: 400
    },
    //show avg data 
    avgWrapper: {
        flexDirection: 'row',
        backgroundColor: '#0F2E45',
        height: 56,
    },
    avgRow: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rowHead: {
        fontSize: 11,
        color:'#afafaf'
    },
    avgShow: {
        fontSize: 14,
        fontFamily: 'Poppins-Medium',
        color:'#ffff'
    }

})