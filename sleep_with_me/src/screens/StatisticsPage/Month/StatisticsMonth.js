import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { LineChart, } from "react-native-chart-kit";
import { BarIndicator } from 'react-native-indicators';
//redux
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
//css style
import styles from './css/GraphStyles';
//screens
import GraphMonth from './GraphMonth';
import QualityMonth from './QualityMonth';
import ResultMonth from './ResultMonth';

export default StatisticsMonth = () => {
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(true)
    const token = useSelector(state => state.auth)

    console.log('token for statistics  ', token)
    const iniData = [{
        id: 1,
        lapTime: 1,
        startDate: "no data",
        startTime: 1,
        stopDate: "no data",
        stopTime: 1
    },
    {
        id: 2,
        lapTime: 1,
        startDate: "no data",
        startTime: 10,
        stopDate: "no data",
        stopTime: 5
    },
    {
        id: 3,
        lapTime: 1,
        startDate: "no data",
        startTime: 8,
        stopDate: "no data",
        stopTime: 2
    },
    {
        id: 4,
        lapTime: 1,
        startDate: "no data",
        startTime: 9,
        stopDate: "no data",
        stopTime: 4
    }]

    const fetchStatMonthData = () => {
        axios.get('http://34.234.203.236:8080/member/statistics', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(response => {
            if (response.data.length <= 0) {
                console.log("mmmmmmmmmmmmm22222");
                dispatch({ type: 'GET_STATMONTH', payload: iniData })
            } else {
                console.log("mmmmmmmmmmmmm33333");
                // console.log('res data month', response.data)
                dispatch({ type: 'GET_STATMONTH', payload: response.data })
            }
            setLoading(false)
        }).catch(error => {
            if (error.response) {
                setLoading(false)
                console.log('error fetch  stat month : ', error)
            }
            else {
                alert('Cannot connect internet')
            }
        })
    }

    useEffect(() => {
        fetchStatMonthData()
    }, [])

    //set loading page
    if (loading) {
        return <View style={styles.loading}>
            <BarIndicator color='white' count={5} />
        </View>
    }

    return (
        <View>
            {/* graph history */}
            <GraphMonth />

            {/* sleep quality */}
            <QualityMonth />

            {/* sleep result */}
            <ResultMonth />
        </View>
    )
}