import React from 'react';
import { View, } from 'react-native';
import { Text } from 'react-native-paper';
import { LineChart, } from "react-native-chart-kit";
//redux
import { useSelector } from 'react-redux';
//css style
import styles from './css/GraphStyles';

export default GraphMonth = () => {
    const statMonth = useSelector(stat => stat.statistics.statMonth)

    // cal averagesSleepTime
    const averagesSleepTime = [...statMonth.reduce((data, currentValue) => {
        let key = currentValue.startDate.slice(4, 7)
        // console.log('key', key)
        let item = data.get(key) || { key, startTime: [] }
        item.startTime.push(currentValue.startTime)
        return data.set(key, item)
    }, new Map).values()]
        .map(({ key, startTime }) => ({
            startDate: key,
            startTime: startTime.reduce((a, b) => a + b) / startTime.length // calculate startTime average
        }))
    const sleepYLabel = averagesSleepTime.map((dataYS) => {
        let startDate = new Date(dataYS.startTime * 1000)
        let startHours = startDate.getHours()
        let startMinutes = "0" + startDate.getMinutes()
        let formattedStartTime = parseFloat(startHours + '.' + startMinutes.substr(-2))
        return formattedStartTime
    })
    const sleepXLabel = averagesSleepTime.map((dataX) => {
        return dataX.startDate
    })


    // cal averagesAwakeTime
    const averagesAwakeTime = [...statMonth.reduce((data, currentValue) => {
        let key = currentValue.stopDate.slice(4, 7)
        // console.log('key', key)
        let item = data.get(key) || { key, stopTime: [] }
        item.stopTime.push(currentValue.stopTime)
        return data.set(key, item)
    }, new Map).values()]
        .map(({ key, stopTime }) => ({
            stopDate: key,
            stopTime: stopTime.reduce((a, b) => a + b) / stopTime.length // calculate startTime average
        }));
    const awakeYLabel = averagesAwakeTime.map((dataYS) => {
        let startDate = new Date(dataYS.stopTime * 1000)
        let startHours = startDate.getHours()
        let startMinutes = "0" + startDate.getMinutes()
        let formattedStartTime = parseFloat(startHours + '.' + startMinutes.substr(-2))
        return formattedStartTime
    })
    const awakeXLabel = averagesAwakeTime.map((dataX) => {
        return dataX.stopDate
    })

    //sort array
    const sleepTimeFormat = statMonth.map((dataYS) => {
        // let unix_timestamp = Math.round((new Date()).getTime() / 1000);
        let startDate = new Date(dataYS.startTime * 1000);
        let startHours = startDate.getHours();
        let startMinutes = "0" + startDate.getMinutes();
        let formattedStartTime = startHours + ':' + startMinutes.substr(-2)
        return formattedStartTime
    })
    var sortTimeSleep = sleepTimeFormat.sort()
    var highestSleep = sortTimeSleep[sortTimeSleep.length - 1]
    var lowestSleep = sortTimeSleep[0]


    const awakeTimeFormat = statMonth.map((dataYS) => {
        // let unix_timestamp = Math.round((new Date()).getTime() / 1000);
        let startDate = new Date(dataYS.stopTime * 1000);
        let startHours = startDate.getHours();
        let startMinutes = "0" + startDate.getMinutes();
        let formattedStartTime = startHours + ':' + startMinutes.substr(-2)
        return formattedStartTime
    })
    var sortTimeAwake = awakeTimeFormat.sort()
    var highestAwake = sortTimeAwake[sortTimeAwake.length - 1]
    var lowestAwake = sortTimeAwake[0]

    //avrage sleep
    const avgSleep = Math.round(sleepYLabel.reduce((total, value) => total + value, 0) / sleepYLabel.length)
    var decSleep = avgSleep - Math.round(avgSleep)
    var avgStringFormat = ("0" + (avgSleep - decSleep)).slice(-2) + ":" + decSleep.toFixed(2).substr(2)

    const avgAwake = Math.round(awakeYLabel.reduce((total, value) => total + value, 0) / awakeYLabel.length)
    var decAwake = avgAwake - Math.round(avgAwake)
    var avgStringFormat2 = ("0" + (avgAwake - decAwake)).slice(-2) + ":" + decAwake.toFixed(2).substr(2)

    //graph data 
    const sleepData = {
        labels: sleepXLabel.reverse(),
        datasets: [
            {
                data: sleepYLabel.reverse(),
                color: (opacity = 1) => `rgba(49, 94, 226, ${opacity})`, // optional
                strokeWidth: 3 // optional
            }
        ]
    }
    const awakeData = {
        labels: awakeXLabel.reverse(),
        datasets: [
            {
                data: awakeYLabel.reverse(),
                color: (opacity = 1) => `rgba(49, 94, 226, ${opacity})`, // optional
                strokeWidth: 3 // optional
            }
        ]
    }
    const chartConfig = {
        backgroundGradientFrom: 'rgba(25, 55, 77, 0.97)',
        backgroundGradientFromOpacity: 0,
        backgroundGradientToOpacity: 0.5,
        color: (opacity = 1) => `rgba(239, 239, 239, ${opacity})`,
        strokeWidth: 2, // optional, default 3
        barPercentage: 0.5,
        propsForDots: {
            r: "5",
            strokeWidth: "2",
        },
    }

    return (
        <View style={styles.graphContainer}>
            <View>
                <View style={styles.headerContent}>
                    <Text style={styles.contentText}>Went to bed : </Text>
                </View>
                <View style={styles.graphView}>
                    <LineChart
                        data={sleepData}
                        width={352}
                        height={234}
                        verticalLabelRotation={30}
                        chartConfig={chartConfig}
                        bezier
                        fromZero={true}
                    />
                </View>
                <View style={styles.avgWrapper}>
                    <View style={styles.avgRow}>
                        <Text style={styles.rowHead}>Highest </Text>
                        <Text style={styles.avgShow}>{highestSleep}</Text>
                    </View>
                    <View style={styles.avgRow}>
                        <Text style={styles.rowHead}>Lowest </Text>
                        <Text style={styles.avgShow}>{lowestSleep}</Text>
                    </View>
                    <View style={styles.avgRow}>
                        <Text style={styles.rowHead}>Average </Text>
                        <Text style={styles.avgShow}>{avgStringFormat} </Text>
                    </View>
                </View>
            </View>
            <View>
                <View style={styles.headerWrapper}>
                    <Text style={styles.contentText}>Awake : </Text>
                </View>
                <View style={styles.graphView}>
                    <LineChart
                        data={awakeData}
                        width={352}
                        height={234}
                        verticalLabelRotation={30}
                        chartConfig={chartConfig}
                        bezier
                        fromZero={true}
                    />
                </View>
                <View style={styles.avgWrapper}>
                    <View style={styles.avgRow}>
                        <Text style={styles.rowHead}>Highest </Text>
                        <Text style={styles.avgShow}>{highestAwake}</Text>
                    </View>
                    <View style={styles.avgRow}>
                        <Text style={styles.rowHead}>Lowest </Text>
                        <Text style={styles.avgShow}>{lowestAwake}</Text>
                    </View>
                    <View style={styles.avgRow}>
                        <Text style={styles.rowHead}>Average </Text>
                        <Text style={styles.avgShow}>{avgStringFormat2} </Text>
                    </View>
                </View>
            </View>
        </View>
    )
}