import React, { useState, useRef, useEffect } from 'react'
import { View, ImageBackground, FlatList, Image, TouchableOpacity, Share, Modal, Alert } from 'react-native';
import { Card, FAB, Button, Text, TextInput, List } from 'react-native-paper';
import RBSheet from "react-native-raw-bottom-sheet";
import SafeAreaView from 'react-native-safe-area-view';
import Icons from 'react-native-vector-icons/Ionicons'
//css styles
import styles from './Styles';
import axios from 'axios';
import { useSelector } from 'react-redux';

export default MyPost = () => {
    const token = useSelector(state => state.auth)
    const userInfo = useSelector(state => state.user)
    // console.log('userInfo ==', userInfo)
    const [refreshing, setRefreshing] = useState(false)

    const [post, setPost] = useState("")

    const [myPost, setMyPost] = useState([])
    const [postId, setPostId] = useState(0)

    const refRBSheet = useRef()
    const [visibleModal, setVisibleModal] = useState(false)

    const showModal = (item) => {
        setVisibleModal(true)
        console.log('item id ==', item.id)
        console.log('item status ==', item.status)

        setPostId(item.id)
        setPost(item.status)
    }

    const closeModal = () => {
        setVisibleModal(false)
        refRBSheet
        setPost("")
    }

    const onShare = async (item) => {
        try {
            const result = await Share.share({
                message: item.status
            })
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    }
    const handleDeletePost = (item) => {
        console.log('item id ==', item.id)
        Alert.alert(
            "Delete Confirm",
            "Are you sure to delete this post?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "OK", onPress: () => {
                        axios.delete('http://34.234.203.236:8080/member/post/' + item.id, {
                            headers: {
                                Authorization: `Bearer ${token}`
                            }
                        }).then(() => {
                            const filter = myPost.filter((t) => t.id != item.id)
                            setMyPost(filter)
                            refRBSheet
                        }).catch(error => {
                            console.log('error fetch my post', error)
                        })
                    }
                }
            ],
            { cancelable: false }
        )
    }

    const handleEditStatusPost = () => {
        axios.patch('http://34.234.203.236:8080/member/post/' + postId, { status: post }, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(() => {
            setVisibleModal(false)
            refRBSheet
        }).catch(error => {
            console.log('error fetch my post', error)
        })
    }

    const fetchMyPost = () => {
        setRefreshing(true)
        axios.get('http://34.234.203.236:8080/member/posts', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(response => {
            setMyPost(response.data)
            setRefreshing(false)
        }).catch(error => {
            console.log('error fetch my post', error)
        })
    }


    useEffect(() => {
        fetchMyPost()
    }, [])

    return (
        <SafeAreaView style={styles.safeArea} >
            <ImageBackground source={require('../../assets/post_bg.png')} style={styles.imageBg}>

                <View style={styles.container}>
                    <View>
                        <FlatList
                            refreshing={refreshing}
                            onRefresh={() => fetchMyPost()}
                            data={myPost}
                            renderItem={({ item }) => (
                                <View>
                                    <Card style={styles.card}>
                                        <View style={styles.cardPostContain}>
                                            <View style={styles.flex1}>
                                                <Image source={{ uri: 'https://pbs.twimg.com/profile_images/1190801752188215297/Mnaw5tT3_400x400.jpg' }}
                                                    style={styles.userImage} />
                                            </View>
                                            <View style={styles.flex3}>
                                                <Text style={styles.usernameText}>{userInfo.username}</Text>
                                                <Text style={styles.emailText}>{userInfo.email}</Text>
                                            </View>
                                            <View style={styles.share}>
                                                <TouchableOpacity
                                                    onPress={() => { refRBSheet.current.open() }}
                                                >
                                                    <Icons name='ios-arrow-down' size={20} color='#A6A6A6' />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={styles.statusContain}>
                                            <Text style={{ color: '#ffff' }}>{item.status}</Text>
                                        </View>
                                        <View style={styles.footerContain}>
                                            <View style={styles.flex1}>
                                                <Text style={styles.timeDate}>07:05:14</Text>
                                            </View>
                                            <View style={styles.flex3}>
                                                <Text style={styles.timeDate}>2020-06-11</Text>
                                            </View>
                                            <View style={styles.share}>
                                                <TouchableOpacity
                                                    onPress={() => onShare(item)}>
                                                    <Icons name='md-share' color='#ffff' size={20} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </Card>

                                    <View>
                                        <RBSheet
                                            ref={refRBSheet}
                                            height={144}
                                            duration={100}
                                            customStyles={{
                                                container: {
                                                    justifyContent: "center",
                                                    backgroundColor: '#0E2639'
                                                }
                                            }}>
                                            <View style={{ borderBottomWidth: 1, borderColor: '#1A364D', padding: 8 }}>
                                                <TouchableOpacity
                                                    onPress={() => showModal(item)}
                                                >
                                                    <List.Item
                                                        titleStyle={{ color: '#FAFAFA' }}
                                                        title="Edit post"
                                                        left={() => <Icons name='ios-create' color='#ffff' size={24} style={{ padding: 8 }} />}
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ borderBottomWidth: 1, borderColor: '#1A364D', padding: 8 }}>
                                                <TouchableOpacity
                                                    onPress={() => handleDeletePost(item)}
                                                >
                                                    <List.Item
                                                        titleStyle={{ color: '#FAFAFA' }}
                                                        title="Delete post"
                                                        left={() => <Icons name='md-trash' color='#ffff' size={26} style={{ padding: 8 }} />}
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </RBSheet>
                                    </View>

                                </View>
                            )}
                            keyExtractor={(item, index) => index + ''}
                        />
                    </View>
                </View>

                <View>
                    <Modal
                        animationType='slide'
                        visible={visibleModal}
                        formSheet
                    >
                        <View style={styles.modalContain}>
                            <View style={styles.modalHead}>
                                <View style={styles.closeIcon}>
                                    <TouchableOpacity
                                        onPress={() => closeModal()}>
                                        <Icons name="ios-close" size={36} color='#46C7F0' />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.postIcon}>
                                    <Button
                                        mode='text'
                                        onPress={() => handleEditStatusPost()}
                                    >
                                        Edit
                                    </Button>
                                </View>
                            </View>
                            <View>
                                <TextInput
                                    multiline
                                    numberOfLines={14}
                                    mode='flat'
                                    value={post}
                                    placeholder='Share your experience dreams . . . '
                                    style={{ backgroundColor: '#1A364D' }}
                                    onChangeText={post => setPost(post)}
                                    onSubmitEditing={() => postInput.current.focus()}
                                    theme={{
                                        colors: {
                                            placeholder: '#A6A6A6',
                                            underlineColor: 'transparent',
                                            text: '#ffff',
                                        }
                                    }}
                                />
                            </View>
                        </View>
                    </Modal>
                </View>
            </ImageBackground>
        </SafeAreaView>
    )
}