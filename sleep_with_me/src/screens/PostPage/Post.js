import React, { useState, useRef, useEffect } from 'react'
import { View, ImageBackground, FlatList, Image, TouchableOpacity, Share, Modal } from 'react-native';
import { Card, FAB, Button, Text, TextInput } from 'react-native-paper';
import SafeAreaView from 'react-native-safe-area-view';
import Icons from 'react-native-vector-icons/Ionicons'
//css styles
import styles from './Styles';
import axios from 'axios';
import { useSelector } from 'react-redux';

export default Post = () => {
    const [post, setPost] = useState("")
    const [refreshing, setRefreshing] = useState(false)

    const [visibleModal, setVisibleModal] = useState(false)
    const token = useSelector(state => state.auth)

    const [listPost, setListPost] = useState([])
    const showModal = () => {
        setVisibleModal(true)
    }
    const closeModal = () => {
        setVisibleModal(false)
        setPost("")
    }

    const onShare = async (item) => {
        try {
            const result = await Share.share({
                message: item.Status,
            })
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    }

    const fetchAllPosts = () => {
        // setRefreshing(true)
        axios.get('http://34.234.203.236:8080/posts')
            .then(response => {
                setListPost(response.data)
            })
            .catch(error => {
                console.log('error to fetch all post = ', error)
            })
    }

    const createStatusPost = () => {
        axios.post('http://34.234.203.236:8080/member/post', { status: post }, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(() => {
            setVisibleModal(false)
            setPost("")
        }).catch(error => {
            console.log('error craete sattus post =', error)
        })
    }

    useEffect(() => {
        fetchAllPosts()
    }, [listPost])

    return (
        <SafeAreaView style={styles.safeArea} forceInset={{ top: 'always' }}  >
            <ImageBackground source={require('../../assets/post_bg.png')} style={styles.imageBg}>

                <View style={styles.headerContain}>
                    <Text style={styles.headerText}>Feed</Text>
                </View>
                <View style={styles.container}>

                    <View>
                        <FlatList
                            refreshing={refreshing}
                            onRefresh={() => fetchAllPosts()}
                            data={listPost}
                            renderItem={({ item }) => (

                                <Card style={styles.card}>
                                    <View style={styles.cardPostContain}>
                                        <View style={styles.flex1}>
                                            <Image source={{ uri: 'https://www.lococrossfit.com/wp-content/uploads/2019/02/user-icon-300x300.png' }}
                                                style={styles.userImage} />
                                        </View>
                                        <View style={styles.flex3}>
                                            <Text style={styles.usernameText}>{item.User.username} </Text>
                                            <Text style={styles.emailText}>{item.User.email} </Text>
                                        </View>
                                    </View>
                                    <View style={styles.statusContain}>
                                        <Text style={{ color: '#ffff' }}>{item.Status}
                                        </Text>
                                    </View>
                                    <View style={styles.footerContain}>
                                        <View style={styles.flex1}>
                                            <Text style={styles.timeDate}>{item.CreatedAt.slice(11, -1)}</Text>
                                        </View>
                                        <View style={styles.flex3}>
                                            <Text style={styles.timeDate}>{item.CreatedAt.slice(0, 10)}</Text>
                                        </View>
                                        <View style={styles.share}>
                                            <TouchableOpacity
                                                onPress={() => onShare(item)}>
                                                <Icons name='md-share' color='#ffff' size={20} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Card>
                            )}
                            keyExtractor={(item, index) => index + ''}
                        // extraData={selected}
                        />
                    </View>
                </View>


                <View style={{ margin: 8 }}>
                    <FAB
                        icon="plus"
                        color='#ffff'
                        style={styles.fab}
                        onPress={showModal}
                    />
                </View>

                <View>

                    <Modal
                        animationType='slide'
                        visible={visibleModal}>

                        <View style={styles.modalContain}>
                            <View style={styles.modalHead}>
                                <View style={styles.closeIcon}>
                                    <TouchableOpacity
                                        onPress={() => closeModal()}>
                                        <Icons name="ios-close" size={36} color='#46C7F0' />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.postIcon}>
                                    <Button
                                        mode='text'
                                        onPress={() => createStatusPost()}
                                    >
                                        Post
                                    </Button>
                                </View>
                            </View>
                            <View>
                                <TextInput
                                    multiline
                                    numberOfLines={14}
                                    mode='flat'
                                    value={post}
                                    placeholder='Share your experience dreams . . . '
                                    style={{ backgroundColor: '#1A364D' }}
                                    onChangeText={post => setPost(post)}
                                    theme={{
                                        colors: {
                                            placeholder: '#A6A6A6',
                                            underlineColor: 'transparent',
                                            text: '#ffff',
                                        }
                                    }}
                                />
                            </View>
                        </View>
                    </Modal>

                </View>
            </ImageBackground>
        </SafeAreaView>
    )
}