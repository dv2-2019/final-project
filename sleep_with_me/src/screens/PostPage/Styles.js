import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#19354B'
    },
    imageBg: {
        flex: 1
    },
    container: {
        flex: 1,
        paddingVertical: 16,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        backgroundColor: '#46C7F0',

    },
    headerContain: {
        height: 72,
        backgroundColor: '#19354B',
        justifyContent: 'center',
        padding: 16,
        elevation : 1
    },
    headerText: {
        fontSize: 24,
        color: '#ffff',
        fontFamily: 'Poppins-Medium'
    },
    card: {
        backgroundColor: '#19354B',
        marginVertical: 8,
        width: 334,
        borderRadius: 8
    },
    cardPostContain: {
        flexDirection: 'row',
        padding: 16
    },
    flex1: {
        flex: 1
    },
    flex3: {
        flex: 3
    },
    userImage: {
        width: 50,
        height: 50,
        borderRadius: 24
    },
    usernameText: {
        fontFamily: 'Poppins-Medium',
        color: '#ffff'
    },
    emailText: {
        color: '#A1A1A1',
        fontSize: 12
    },
    statusContain: {
        backgroundColor: '#162F42',
        padding: 16
    },
    footerContain: {
        flexDirection: 'row',
        padding: 16
    },
    timeDate: {
        color: '#A1A1A1',
        fontSize: 13
    },
    share: {
        flex: 1,
        alignItems: 'flex-end'
    },
    modalContain: {
        flex: 1,
        backgroundColor: 'rgba(14,38,57,0.98)'
    },
    modalHead: {
        flexDirection: 'row',
        marginVertical: 8,
        marginHorizontal: 16
    },
    closeIcon: {
        flex: 1,
        alignItems: 'flex-start'
    },
    postIcon: {
        flex: 1,
        alignItems: 'flex-end'
    }



})