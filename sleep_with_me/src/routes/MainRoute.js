import React from 'react';
import { Button, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//screen

import BottomTab from './BottomTab';
import EditProfile from '../screens/EditProfilePage/EditProfile';
import MainStat from '../screens/StatisticsPage/MainStat';
import SwipeStop from '../screens/HomePage/SwipeStop';
import MyPost from '../screens/MyPostPage/MyPost';

const Stack = createStackNavigator();

export default MainRoute = () => {
    return (
            <Stack.Navigator
                initialRouteName="BottomTab">
                <Stack.Screen
                    name="BottomTab"
                    component={BottomTab}
                    options={{
                        headerShown: false,
                    }} />
                <Stack.Screen
                    name="SwipeStop"
                    component={SwipeStop}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="EditProfile"
                    component={EditProfile}
                    options={{
                        headerStyle: {
                            elevation: 2, backgroundColor: '#0E2639'
                        },
                        headerTitleStyle: {
                            fontFamily: 'Poppins-Regular', fontSize: 18
                        },
                        title: 'Edit Profile',
                        headerTintColor: '#ffff',
                    }}
                />
                <Stack.Screen
                    name="MyPost"
                    component={MyPost}
                    options={{
                        headerStyle: {
                            elevation: 2, backgroundColor: '#0E2639'
                        },
                        headerTitleStyle: {
                            fontFamily: 'Poppins-Regular', fontSize: 18
                        },
                        title: 'My Post',
                        headerTintColor: '#ffff',
                    }}
                />
                <Stack.Screen
                    name="MainStat"
                    component={MainStat}
                    options={{
                        headerStyle: {
                            elevation: 2
                        },
                        title: null
                    }}
                />
            </Stack.Navigator>
    )
}
