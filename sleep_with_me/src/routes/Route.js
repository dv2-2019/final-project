import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//screen
import AuthRoute from './AuthRoute';
import MainRoute from './MainRoute';
import { useSelector } from 'react-redux'
const Stack = createStackNavigator();

export default Route = () => {
    const token = useSelector(state => state.auth)
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                {
                    !token ?
                        <Stack.Screen name="AuthRoute" component={AuthRoute} />
                        :
                        <Stack.Screen name="MainRoute" component={MainRoute} />
                }
            </Stack.Navigator>
        </NavigationContainer>
    )
}
