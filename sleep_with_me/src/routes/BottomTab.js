import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icons from 'react-native-vector-icons/Ionicons'

//stack screen
import Home from '../screens/HomePage/Home';
import Optional from '../screens/OptionalPage/Optional';
import MainStat from '../screens/StatisticsPage/MainStat';
import Post from '../screens/PostPage/Post';

const Tab = createBottomTabNavigator();

export default BottomTab = () => {

    return (

        <Tab.Navigator
            tabBarOptions={{
                keyboardHidesTabBar: true,
                activeTintColor: '#46C7F0',
                inactiveTintColor: '#bebebe',
                keyboardHidesTabBar: true,
                tabStyle: {
                    backgroundColor: '#0C2334',
                    elevation: 6,
                },
                labelStyle: {
                    fontFamily: 'Poppins-Regular',
                },
                style: {
                    borderTopColor: '#0C2334',
                    elevation: 6,
                }
            }}>

            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarLabel: 'Sleep',
                    tabBarIcon: ({ color, size }) => (
                        <Icons name="ios-cloudy-night" color={color} size={size} />
                    ),
                }}
            />

            <Tab.Screen
                name="MainStat"
                component={MainStat}
                options={{
                    tabBarLabel: 'Statistics',
                    tabBarIcon: ({ color, size }) => (
                        <Icons name="ios-stats" color={color} size={size} />
                    ),
                }}
            />

            <Tab.Screen
                name="Post"
                component={Post}
                options={{
                    tabBarLabel: 'Post',
                    tabBarIcon: ({ color, size }) => (
                        <Icons name="ios-bulb" color={color} size={size} />
                    ),
                }}
            />

            <Tab.Screen
                name="Optional"
                component={Optional}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color, size }) => (
                        <Icons name="md-person" color={color} size={size} />
                    ),
                }}
            />

        </Tab.Navigator>

    );
}
