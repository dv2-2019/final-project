import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import Login from '../screens/LoginPage/Login'
import Register from '../screens/RegisterPage/Register'
import SlideStart from '../screens/StartPage/SlideStart'

const Stack = createStackNavigator()

export default () => {
    return (
            <Stack.Navigator initialRouteName="SlideStart">
                <Stack.Screen name="SlideStart" component={SlideStart} options={{ headerShown: false }} />
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen
                    name="Register"
                    component={Register}
                    options={{
                        headerStyle: {
                            elevation: 0,
                            backgroundColor:'#0F2D44'
                        },
                        title: null,
                        headerTintColor: '#ffff'
                    }}
                />
            </Stack.Navigator>
    )
}
