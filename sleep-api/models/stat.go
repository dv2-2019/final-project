package models

import "github.com/jinzhu/gorm"

type Statistic struct {
	gorm.Model
	LapTime   int
	StartTime int
	StopTime  int
	StartDate string
	StopDate  string
	UserID    uint
}

type StatisticService interface {
	CreateStat(statistic *Statistic) error
	ListStatByUserID(id uint) ([]Statistic, error)
}

type statisticGorm struct {
	db *gorm.DB
}

func NewStatisticService(db *gorm.DB) StatisticService {
	return &statisticGorm{db: db}
}

func (sg *statisticGorm) CreateStat(statistic *Statistic) error {
	return sg.db.Create(statistic).Error
}

func (sg *statisticGorm) ListStatByUserID(id uint) ([]Statistic, error) {
	listStat := []Statistic{}
	if err := sg.db.
		Where("user_id = ?", id).
		Find(&listStat).Error; err != nil {
		return nil, err
	}
	return listStat, nil
}
