package models

import (
	"github.com/jinzhu/gorm"
)

type ProfileImg struct {
	gorm.Model
	UserID   uint   `gorm:"not null"`
	Filename string `gorm:"not null"`
}
type ProfileImgService interface {
	CreateProfile(profileImg *ProfileImg) error
	UpdateImage(id uint, filename string) error
}

type profileImageService struct {
	db *gorm.DB
}

func NewProfileImageService(db *gorm.DB) ProfileImgService {
	return &profileImageService{db}
}

func (pg *profileImageService) CreateProfile(profileImg *ProfileImg) error {
	return pg.db.Create(profileImg).Error
}

func (ims *profileImageService) UpdateImage(id uint, filename string) error {
	return ims.db.Model(&ProfileImg{}).Where("user_id = ?", id).Update("filename", filename).Error
}

// func (ims *profileImageService) GetByUserID(id uint) (*ProfileImg, error) {
// 	image := ProfileImg{}
// 	if err := ims.db.
// 		Where("user_id = ?", id).
// 		Find(&image).Error; err != nil {
// 		return nil, err
// 	}
// 	return image, nil
// }
