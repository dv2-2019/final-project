package models

///////////// models คือการ create db /////////////
import (
	"fmt"
	"log"
	"sleep-api/hash"
	"sleep-api/rand"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const cost = 12

type User struct {
	gorm.Model
	Email      string      `gorm:"unique_index;not nul" json:"email"`
	Username   string      `gorm:"unique_index;not nul" json:"username"`
	Password   string      `gorm:"not nul"`
	Token      string      `gorm:"unique_index"`
	ProfileImg *ProfileImg `gorm:"column:profile_imgs;one2one:profile_imgs;foreignkey:user_id;association_foreignkey:id"`
	//one to one ต้องใส่ pointer เพื่อจะได้รู้ว่าส่งขอข้อมูลจากใคร
}

type userGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

type UserService interface {
	CreateUser(user *User) error
	UserLogin(user *User) (string, error)
	UpdateUserProfile(id uint, username string, email string, password string) (err error)

	GetUserByID(id uint) (*User, error)
	GetUserByToken(token string) (*User, error)
	Logout(user *User) error
}

func NewUserService(db *gorm.DB, hmac *hash.HMAC) UserService {
	return &userGorm{db, hmac}
}

//ug = userGorm เพื่อสร้างข้อมูลไป db
func (ug *userGorm) CreateUser(temp *User) error {
	//สร้าง user ใหม่โดยรับค่าต้นมาจาก user struct
	user := new(User)
	user.Email = temp.Email
	user.Username = temp.Username
	user.Password = temp.Password

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}

	//get hash token
	fmt.Println("create token ===> ", token)
	tokenHash := ug.hmac.Hash(token)
	fmt.Println("create tokenHashStr ===> ", tokenHash)

	//replace hashtoken str to old user token
	user.Token = tokenHash
	temp.Token = token

	return ug.db.Create(user).Error //PUSH data to db

}

func (ug *userGorm) UserLogin(user *User) (string, error) {
	foundUser := new(User)
	err := ug.db.Where("username = ?", user.Username).First(&foundUser).Error
	if err != nil {
		return "", err
	}
	//รับ พาสเวิดมาแล้ว compare
	err = bcrypt.CompareHashAndPassword([]byte(foundUser.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}
	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	//get hash token
	fmt.Println("login token ===> ", token)
	tokenHash := ug.hmac.Hash(token)
	fmt.Println("login tokenHashStr ===> ", tokenHash)

	//add token
	err = ug.db.Model(&User{}).Where("id = ?", foundUser.ID).Update("token", tokenHash).Error
	if err != nil {
		return "", err
	}
	return token, nil

}
func (ug *userGorm) GetUserByToken(token string) (*User, error) {
	//hash token lookup
	tokenHash := ug.hmac.Hash(token)
	log.Println("lookup for user by token(hashed): ", tokenHash)

	user := new(User)
	err := ug.db.Preload("ProfileImg").Where("token = ?", tokenHash).First(user).Error
	if err != nil {
		return user, err
	}

	return user, err
}

func (ug *userGorm) Logout(user *User) error {
	return ug.db.Model(user).
		Where("id = ?", user.ID).
		Update("token", gorm.Expr("NULL")).Error
}

func (ug *userGorm) UpdateUserProfile(id uint, username string, email string, password string) (err error) {

	if username != "" {
		if err := ug.db.Model(&User{}).Where("id = ?", id).Update("username", username).Error; err != nil {
			return err
		}
	}

	if email != "" {
		if err := ug.db.Model(&User{}).Where("id = ?", id).Update("email", email).Error; err != nil {
			return err
		}
	}

	if password != "" {
		if err := ug.db.Model(&User{}).Where("id = ?", id).Update("password", password).Error; err != nil {
			return err
		}
	}
	return err
	// return ug.db.Model(&User{}).Where("id = ?", id).Updates(map[string]interface{}{"username": username, "email": email, "password": password}).Error
}

func (ug *userGorm) GetUserByID(id uint) (*User, error) {
	//get by idrelate with image
	user := new(User)
	user.ID = id
	if err := ug.db.Preload("ProfileImg").First(user).Error; err != nil {
		return nil, err
	}
	return user, nil

}
