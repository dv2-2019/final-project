package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type Post struct {
	gorm.Model
	UserID uint `gorm:"column:user_id"`
	User   User `gorm:"column:user"`
	Status string
}

type PostService interface {
	CreatePost(post *Post) error
	UpdatePost(id uint, status string) error
	DeletePost(id uint) error
	//list
	ListByUserID(id uint) ([]Post, error)
	ListPosts() ([]Post, error)
}

type postGorm struct {
	db *gorm.DB
}

func NewPostService(db *gorm.DB) PostService {
	return &postGorm{db}
}

func (pg *postGorm) CreatePost(post *Post) error {
	return pg.db.Create(post).Error
}

func (pg *postGorm) UpdatePost(id uint, status string) error {
	return pg.db.Model(&Post{}).Where("id = ?", id).Update("status", status).Error
}

func (pg *postGorm) DeletePost(id uint) error {
	return pg.db.Where("id = ?", id).Delete(&Post{}).Error
}

func (pg *postGorm) ListByUserID(id uint) ([]Post, error) {
	posts := []Post{}
	if err := pg.db.
		Where("user_id = ?", id).
		Find(&posts).Error; err != nil {
		return nil, err
	}
	return posts, nil
}

func (pg *postGorm) ListPosts() ([]Post, error) {
	postList := []Post{}
	//group user width post
	rows, err := pg.db.Table("posts").
		Select("posts.id, posts.user_id, posts.status, posts.created_at, users.id, users.email, users.username").
		Joins("join users on users.id = posts.user_id").
		Where("posts.deleted_at is NULL").
		Rows()

	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := Post{}
		err := rows.Scan(&report.ID, &report.UserID, &report.Status, &report.CreatedAt, &report.User.ID, &report.User.Email, &report.User.Username)
		if err != nil {
			fmt.Println(err)
		}
		postList = append(postList, report)
	}

	fmt.Println("RESULT POST WITH USER ====== ", postList)
	return postList, nil
}
