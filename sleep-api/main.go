package main

import (
	"log"
	"sleep-api/config"
	"sleep-api/handlers"
	"sleep-api/hash"
	"sleep-api/models"
	"sleep-api/mw"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {

	//create db from set up in conf
	// db, err := gorm.Open("mysql", "root:password@tcp(127.0.0.1:3305)/sleepApp?charset=utf8&parseTime=True")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer db.Close()
	// db.LogMode(true)

	// err = models.AutoMigrate(db)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true) // dev only!
	}

	err = models.AutoMigrate(db)
	if err != nil {
		log.Fatal(err)
	}

	hmac := hash.NewHMAC(conf.HMACKey)

	//user
	us := models.NewUserService(db, hmac)
	uh := handlers.NewUserHandler(us)

	//profile
	ims := models.NewProfileImageService(db)
	imh := handlers.NewImageHandler(ims)

	//stat
	ss := models.NewStatisticService(db)
	sh := handlers.NewStatisticHandler(ss)

	//post
	ps := models.NewPostService(db)
	ph := handlers.NewPostHandler(ps)

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "PUT", "PATCH", "POST", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	//upload image
	r.Static("/upload", "./upload")

	//auth
	r.POST("/signup", uh.Signup)
	r.POST("/login", uh.Login)

	//post
	r.GET("/posts", ph.ListAllPosts)

	auth := r.Group("/")
	auth.Use(mw.AuthRequired(us))
	{
		auth.POST("/logout", uh.Logout)
		member := auth.Group("/member")
		{
			//user
			member.GET("/profile", uh.GetUserProfile)
			member.PATCH("/profile", uh.UpdateUser)
			member.POST("/profile/image", imh.CreateProfileImage)
			member.PATCH("/profile/image", imh.UpdateProfileImage)

			//statistics
			member.POST("/statistics", sh.CreateStatistic)
			member.GET("/statistics", sh.ListStatistics)

			//post
			member.POST("/post", ph.CreateStatusPost)
			member.PATCH("/post/:id", ph.UpdateStatusPost)
			member.DELETE("/post/:id", ph.DeleteStatusPost)
			member.GET("/posts", ph.ListUserPost)
		}
	}

	r.Run(":8080")
}
