package mw

import (
	"log"
	"sleep-api/models"
	"strings"

	"github.com/gin-gonic/gin"
)

func AuthRequired(us models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		header = strings.TrimSpace(header)
		log.Println("header ===> ", header)
		if len(header) <= 7 {
			c.Status(401)
			c.Abort()
			return
		}
		token := header[len("Bearer "):]
		log.Println("token ===> ", token)
		if token == "" {
			c.Status(401)
			c.Abort()
			return
		}

		user, err := us.GetUserByToken(token)
		if err != nil {
			c.Status(401)
			c.Abort()
		}

		c.Set("user", user)

	}

}
