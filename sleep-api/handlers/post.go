package handlers

import (
	"fmt"
	"sleep-api/context"
	"sleep-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Post struct {
	ID     uint   `json:"id"`
	Status string `json:"status"`
}

type PostHandler struct {
	ps models.PostService
}

func NewPostHandler(ps models.PostService) *PostHandler {
	return &PostHandler{ps}
}

/////////////create post func //////////
type CreatePost struct {
	Status string `json:"status"`
}
type CreateRes struct {
	Post
}

func (ph *PostHandler) CreateStatusPost(c *gin.Context) {
	user := context.User(c) //func แยก รับค่า user มาจาก user models
	if user == nil {
		c.Status(401)
		return
	}

	data := new(CreatePost)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	post := new(models.Post)
	post.Status = data.Status
	post.UserID = user.ID //relationship with user id
	if err := ph.ps.CreatePost(post); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(CreateRes)
	res.ID = post.ID
	res.Status = post.Status
	c.JSON(201, res)
}

///////// update post func //////////
type UpdatePostReq struct {
	Status string `json:"status"`
}

func (ph *PostHandler) UpdateStatusPost(c *gin.Context) {
	statusId := c.Param("id")
	id, err := strconv.Atoi(statusId)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	updateStatus := new(UpdatePostReq)
	if err := c.BindJSON(updateStatus); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = ph.ps.UpdatePost(uint(id), updateStatus.Status)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

////////// delete sattus post ///////////
func (ph *PostHandler) DeleteStatusPost(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr) //ไม่ต้องเช้ค string เปล่าแล้วเพราะถ้าค่ารับเข้ามาแล้วหาไม่เจอก็จะรีเทรินเป็น error และไม่ขัดกับ string converse
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	if err := ph.ps.DeletePost(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

//////////// list post by user id ////////////////
func (ph *PostHandler) ListUserPost(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := ph.ps.ListByUserID(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	posts := []Post{}
	for _, d := range data {
		posts = append(posts, Post{
			ID:     d.ID,
			Status: d.Status,
		})
	}
	c.JSON(200, posts)
}

/////////// list all post //////////////
func (ph *PostHandler) ListAllPosts(c *gin.Context) {
	data, err := ph.ps.ListPosts()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Println("POST DATA FORM MODELS =====", data)
	c.JSON(200, data)
}
