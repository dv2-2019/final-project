package handlers

import (
	"sleep-api/context"
	"sleep-api/models"

	"github.com/gin-gonic/gin"
)

type ImageRes struct {
	ID uint `json:"id"`
	// UserID   uint   `json:"user_id"`
	Filename string `json:"filename"`
}

type ImageHandler struct {
	ims models.ProfileImgService
}

func NewImageHandler(ims models.ProfileImgService) *ImageHandler {
	return &ImageHandler{ims}
}

///////////// create profile func //////////////////
type CreateImageReq struct {
	Filename string `json:"filename"`
}
type CreateImgRes struct {
	ImageRes
}

func (imh *ImageHandler) CreateProfileImage(c *gin.Context) {
	user := context.User(c) //func แยก รับค่า user มาจาก user models
	if user == nil {
		c.Status(401)
		return
	}

	data := new(CreateImageReq)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	image := new(models.ProfileImg)
	image.Filename = data.Filename
	image.UserID = user.ID //relationship with user id
	if err := imh.ims.CreateProfile(image); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(CreateImgRes)
	res.ID = image.ID
	res.Filename = image.Filename
	c.JSON(201, res)
}

/////////// update profile func ////////////
type UpdateImageReq struct {
	Filename string `json:"filename"`
}

func (imh *ImageHandler) UpdateProfileImage(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	updateImage := new(UpdateImageReq)
	if err := c.BindJSON(updateImage); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	err := imh.ims.UpdateImage(user.ID, updateImage.Filename)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}
