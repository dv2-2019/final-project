package handlers

import (
	"sleep-api/context"
	"sleep-api/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type Statistic struct {
	gorm.Model
	ID        uint   `json:"id"`
	LapTime   int    `json:"lapTime"`
	StartTime int    `json:"startTime"`
	StopTime  int    `json:"stopTime"`
	StartDate string `json:"startDate"`
	StopDate  string `json:"stopDate"`
}

type StatisticHandler struct {
	ss models.StatisticService
}

func NewStatisticHandler(ss models.StatisticService) *StatisticHandler {
	return &StatisticHandler{ss}
}

type CreateReq struct {
	Statistic
}

func (sh *StatisticHandler) CreateStatistic(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	req := new(CreateReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	stat := new(models.Statistic)
	stat.LapTime = req.LapTime
	stat.StartTime = req.StartTime
	stat.StopTime = req.StopTime
	stat.StartDate = req.StartDate
	stat.StopDate = req.StopDate
	stat.UserID = user.ID

	if err := sh.ss.CreateStat(stat); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(201, gin.H{
		"lapTime":   stat.LapTime,
		"startTime": stat.StartTime,
		"stopTime":  stat.StopTime,
		"startDate": stat.StartDate,
		"stopDate":  stat.StopDate,
	})

}

func (sh *StatisticHandler) ListStatistics(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	data, err := sh.ss.ListStatByUserID(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	statistics := []Statistic{}
	for _, d := range data {
		statistics = append(statistics, Statistic{
			ID:        d.ID,
			LapTime:   d.LapTime,
			StartTime: d.StartTime,
			StopTime:  d.StopTime,
			StartDate: d.StartDate,
			StopDate:  d.StopDate,
		})
	}
	c.JSON(200, statistics)

}
