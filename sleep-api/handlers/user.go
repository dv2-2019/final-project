package handlers

//////////// hadlers คือฝั่งการรับข้อมูลใาจาก models และนำข้อมูลออกไปหน้าบ้าน
import (
	"sleep-api/context"
	"sleep-api/models"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us models.UserService //handle user med from models
}

type SignupReq struct { //ทางฝั่งของ display ข้อมูลในรูป json ไปหน้าบ้าน
	Email    string `json:email`
	Password string `json:password`
	Username string `json:username`
	Token    string `gorm:"unique_index`
}

func NewUserHandler(us models.UserService) *UserHandler {
	return &UserHandler{us}
}

func (uh *UserHandler) Signup(c *gin.Context) {
	req := new(SignupReq)
	if err := c.BindJSON(req); err != nil {
		c.Status(400)
		return
	}
	//สร้าง user ใหม่โดยรับค่าต้นมาจาก user ที่สร้างใน models
	user := new(models.User)
	user.Email = req.Email
	user.Username = req.Username
	user.Password = req.Password
	if err := uh.us.CreateUser(user); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(201, gin.H{
		"email":    user.Email,
		"username": user.Username,
		"password": user.Password,
	})

}

type LoginReq struct {
	Username string
	Password string
}

func (uh *UserHandler) Login(c *gin.Context) {
	req := new(LoginReq)

	// BindJSON abort the request with HTTP 400 if any error occurs.
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user := new(models.User)
	user.Username = req.Username
	user.Password = req.Password
	token, err := uh.us.UserLogin(user)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (uh *UserHandler) Logout(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "Invalid token",
		})
		return
	}
	err := uh.us.Logout(user)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

type UserProfileReq struct {
	Email    string
	Username string
}

func (uh *UserHandler) GetUserProfile(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.JSON(401, gin.H{
			"message": "Invalid token",
		})
		return
	}

	c.JSON(201, user)
}

type UpdateProfileReq struct {
	Email    string `json:email`
	Password string `json:password`
	Username string `json:username`
}

func (uh *UserHandler) UpdateUser(c *gin.Context) {
	user := context.User(c)

	updateProfile := new(UpdateProfileReq)
	if err := c.BindJSON(updateProfile); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	err := uh.us.UpdateUserProfile(
		user.ID,
		updateProfile.Username,
		updateProfile.Email,
		updateProfile.Password)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)

}
